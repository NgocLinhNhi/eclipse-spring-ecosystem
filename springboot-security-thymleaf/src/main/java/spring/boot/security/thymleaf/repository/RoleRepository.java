package spring.boot.security.thymleaf.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import spring.boot.security.thymleaf.domain.Role;

public interface RoleRepository extends JpaRepository<Role, Integer> {

	Role findByName(String name);

}
