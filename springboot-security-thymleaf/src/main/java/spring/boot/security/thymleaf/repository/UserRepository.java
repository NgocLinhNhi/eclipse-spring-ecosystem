package spring.boot.security.thymleaf.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import spring.boot.security.thymleaf.domain.User;

public interface UserRepository extends JpaRepository<User, Integer> {

	User findByEmail(String email);

}
