package spring.boot.security.thymleaf.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	/*
	 * Trong lớp WebSecurityConfig, ta cần phải gọi đến interface UserDetailsService
	 * để cấu hình. Do đó ta sẽ inject UserDetailsService.
	 */
	@Autowired
	private UserDetailsService userDetailsService;
	
	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
	
	@Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
    }
	
	@Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .authorizeRequests()
            	//pertmitAll = các trang không cần security
                .antMatchers("/register").permitAll()
                //role MEMBER - ADMIN là mặc định của config đổi = die
				/*
				 * Một GrantedAuthority là một quyền được ban cho principal. Các quyền đều có
				 * tiền tố là ROLE_, ví dụ như ROLE_ADMIN, ROLE_MEMBER muốn có quyền nào thì
				 * thêm ROLE_ trc data trong db spring-security tự xác định
				 */
                //tất cả các URI còn lại đòi hỏi role MEMBER
                .antMatchers("/").hasRole("MEMBER")
                //1 URI cho nhiều role đăng nhập vào
                //nếu vào role ko đc phép nó tự redirect về trang login
                .antMatchers("/admin").hasRole("ADMIN") 
                .antMatchers("/admin").hasRole("SIDA")
                .and()
            .formLogin()
            	//.loginProcessingUrl("/userLogin")//set up 1 trang login khac dung post method
            	.loginPage("/login")//trang login mặc định có thể thay đổi
            	.usernameParameter("email")//quy định trong trang login fiedl của text
            	.passwordParameter("password")
            	.defaultSuccessUrl("/")//login success gọi đên URI index
            	//.defaultSuccessUrl("/loginAccess",true)//set up đến 1 URI khác sau khi login success
            	.failureUrl("/login?error")
            	.and()
        	.exceptionHandling()
    			.accessDeniedPage("/403");
    }
	
}
