package spring.boot.security.thymleaf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootSecurityThymleafApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootSecurityThymleafApplication.class, args);
	}

}
