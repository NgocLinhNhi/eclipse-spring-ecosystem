'use strict';
var app = angular.module("UserManagement", []); //UserManagement= ng-app o html


//Controller module
app.controller("UserController", function ($scope, $http) {
    //UserController = ng-controller ở html
    //$scope kho lưu trữ giữ liệu đế quan hệ với view controller
    $scope.product = [];

    $scope.loginUser = function () {
        // this này tượng trưng cho đọc được hết field bên form
        //or var user = $scope.user;
        var vm = this;
        var user = vm.user;

        $http({
            method: 'POST',
            url: '/user/login',
            params: {
                loginName: user.loginName,
                password: user.password
            },
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(sucess, error);
    };

    function sucess() {
        //Success gọi đến trang product.html
    	window.location.href = "/pages/product.html";
       // window.location.href = "/pages/productCRUDBasic.html";
    }

    function error(res) {
        var $notify_modal = $("#notifiModal .modal-header");
        var $notify =$("#notifiModal");

        $notify_modal.removeClass("btn-success");
        $notify_modal.addClass("btn-danger");
        $notify.addClass("fade");
        $('#massagemodal').html(res.data.message);//ko cần như Jquery responseJSON.message mới hiển thị được
        $notify.modal("toggle");
        $notify.on('hidden.bs.modal', function () {
            $(".modal-backdrop").remove();
            $("#notifiModal").removeAttr("style");
        });
    }

    $scope.reset = function () {
        $("#loginName").val('');
        $("#password").val('');
    };

});