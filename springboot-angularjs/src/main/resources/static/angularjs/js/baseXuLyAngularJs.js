'use strict';
var app = angular.module("productManagement", []);

// Controller Part
//Flow xử lý không tách service
app.controller("ProductController", function($scope, $http) {
	loadAllData();

	function loadAllData() {
		$http({
			method : 'GET',
			url : '/product/loadAllProduct'
		}).then(function(res) {
			$scope.product = res.data;
		}, function(res) {
			console.log("Error: " + res.status + " : " + res.data);
		});
	}

	$scope.addProduct = function() {
		var vm = this;
		var productAdd = {
			"productName" : vm.product.productName,
			"category" : {
				"categoryId" : 1
			},
			"price" : vm.product.price,
			"imageProduct" : "123",
			"numberSales" : vm.product.numberSales,
			"guarantee" : 20
		};

		// response body thì phải truyền kiểu này
		$http.post('/product/addNewProduct', productAdd).then(sucess, error);
	};

	$scope.deleteProduct = function(pro) {
		var seqPro = pro.seqPro;

		// truyền tham số thì có thể xử lý được cả 2 kiểu $http.post(...
		$http({
			// method: 'DELETE',
			method : 'POST',
			// url: '/product/deleteProductAngular/' + seqPro
			url : '/product/deleteProduct/' + seqPro
		}).then(sucess, error);
	};

	$scope.editProduct = function(pro) {
		var seqPro = pro.seqPro;

		$http({
			method : 'GET',
			url : '/product/findProductBySeq/' + seqPro
		}).then(setInfor, editError);
	};

	var productObjectUpdate;
	function setInfor(data) {
		var myModal = $("#myModal");

		myModal.addClass("fade");
		myModal.modal("toggle");
		$("#myModal.modal-title").html("Edit Product");

		$scope.product = data.data;
		productObjectUpdate = $scope.product;

		$("#seqPro").val($scope.product.seqPro);
		//productNameUpdate do trùng với form add nên không hiển thị  nếu để productName
		$("#productNameUpdate").val($scope.product.productName);
		$("#priceProduct").val($scope.product.price);
		$("#sysStatus").val($scope.product.sysStatus);

		loadAllData();
	}

	function editError() {
		alert('Không tìm thấy data');

	}

	$scope.updateProduct = function() {
		var productNameTrim = $("#productNameUpdate").val().trim();
		var productPrice = $("#priceProduct").val().trim();
		var sysStatusTrim = $("#sysStatus").val().trim();

		var productName = productNameTrim !== "" ? productNameTrim : null;
		var price = productPrice  !== "" ? productPrice : 0;
		var sysStatus = sysStatusTrim!== "" ? sysStatusTrim : null;

		var dataa={
				"seqPro":productObjectUpdate.seqPro,
				//"productName" : vm.productNameUpdate, // "dùng ng-entity sẽ chết khi ko thay đổi giá trị text ma kick update luôn "
				"productName" : productName,
				"price" : price,
				"numberSales" : productObjectUpdate.numberSales,
				"guarantee" : productObjectUpdate.guarantee,
				"imageProduct":productObjectUpdate.imageProduct,
				"category": {
				    "categoryId":1 },
				"sysStatus":sysStatus,
				"createDate":productObjectUpdate.createDate,
				"updateDate":productObjectUpdate.updateDate
				};
		$http.post('/product/updateProduct', dataa).then(sucess, error);

		loadAllData();
	};


	//Thông báo message add - xoa
	function sucess(res) {
		var $notify_modal = $("#notifiModal .modal-header");
		var $notify =$("#notifiModal");

		$notify_modal.removeClass("btn-danger");
		$notify_modal.addClass("btn-success");
		$notify.addClass("fade");
		$('#massagemodal').html(res.data.message);
		$notify.modal("toggle");
		$notify.on('hidden.bs.modal', function() {location.reload();});
	}

	function error(res) {
		var $notify_modal = $("#notifiModal .modal-header");
		var $notify =$("#notifiModal");

		$notify_modal.removeClass("btn-success");
		$notify_modal.addClass("btn-danger");
		$notify.addClass("fade");
		$('#massagemodal').html(res.data.message);
		$notify.modal("toggle");
		$notify.on('hidden.bs.modal', function() {
			$(".modal-backdrop").remove();
			$("#notifiModal").removeAttr("style");
		});
	}

	$scope.reset = function() {
		$("#loginName").val('');
		$("#password").val('');
	};

});