'use strict';

angular.module('productManagement').controller('ProductController',
    ['ProductService', '$scope', function (ProductService, $scope) {

        // gọi hàm onload data
        loadAllProduct();
        var dataUpdate = {};

        function loadAllProduct() {
            // set data cho object product ở trang html
            $scope.product = ProductService.getAllProducts();
            // gọi đến service của angularjs
            return ProductService.getAllProducts();
        }

        //Delete Product Controller
        $scope.deleteProduct = function (pro) {
            var seqPro = pro.seqPro;
            ProductService.deleteProduct(seqPro).then(sucess, error);
        };

        //view Detail Product controller pro này được truyền từ html luôn ko cần phải select database tìm lại đối tượng đó
        $scope.editProduct = function (pro) {
            // var seqPro = pro.seqPro;
            // dataUpdate = ProductService.findProductById(seqPro);
            dataUpdate = pro;
            setInfor();
        };

        function setInfor() {
            var myModal = $("#myModal");

            myModal.addClass("fade");
            myModal.modal("toggle");
            $("#myModal.modal-title").html("Edit Product");
            $("#seqPro").val(dataUpdate.seqPro);
            $("#priceProduct").val(dataUpdate.price);
            $("#productNameUpdate").val(dataUpdate.productName);
            $("#sysStatus").val(dataUpdate.sysStatus);
        }

        //Add Product controller
        $scope.addProduct = function () {
            var vm = this;
            var productAdd = {
                "productName": vm.product.productName,
                "category": {
                    "categoryId": 1
                },
                "price": vm.product.price,
                "imageProduct": "123",
                "numberSales": vm.product.numberSales,
                "guarantee": 20
            };
            ProductService.addProduct(productAdd).then(sucess, error);
        };

        //Update product controller
        $scope.updateProduct = function () {
            var productNameTrim = $("#productNameUpdate").val().trim();
            var productPrice = $("#priceProduct").val().trim();
            var sysStatusTrim = $("#sysStatus").val().trim();

            var productName = productNameTrim !== "" ? productNameTrim : null;
            var sysStatus = sysStatusTrim !== "" ? sysStatusTrim : null;
            var price = productPrice  !== "" ? productPrice : 0;

            var dataa = {
                "seqPro": dataUpdate.seqPro,
                "productName": productName,
                "price": price,
                "numberSales": dataUpdate.numberSales,
                "guarantee": dataUpdate.guarantee,
                "imageProduct": dataUpdate.imageProduct,
                "category": {
                    "categoryId": 1
                },
                "sysStatus": sysStatus,
                "createDate": dataUpdate.createDate,
                "updateDate": dataUpdate.updateDate
            };
            ProductService.updateProduct(dataa).then(sucess, error);
        };

        $scope.reset = function () {
            var vm = this;
            vm.product.productName = "";
            vm.product.price = "";
            vm.product.numberSales = "";
        }

    }
    ]);

        // thong bao message add - xoa
        function sucess(res) {
            var $notify_modal = $("#notifiModal .modal-header");
            var $notify = $("#notifiModal");

            $notify_modal.removeClass("btn-danger");
            $notify_modal.addClass("btn-success");
            $notify.addClass("fade");
            $('#massagemodal').html(res.message);
            $notify.modal("toggle");
            $notify.on('hidden.bs.modal', function () {
                location.reload();
            });
        }

        function error(res) {
            var $notify_modal = $("#notifiModal .modal-header");
            var $notify = $("#notifiModal");

            $notify_modal.removeClass("btn-success");
            $notify_modal.addClass("btn-danger");
            $notify.addClass("fade");
            $('#massagemodal').html(res.message);
            $notify.modal("toggle");
            $notify.on('hidden.bs.modal', function () {
                $(".modal-backdrop").remove();
                $("#notifiModal").removeAttr("style");
            });
}
