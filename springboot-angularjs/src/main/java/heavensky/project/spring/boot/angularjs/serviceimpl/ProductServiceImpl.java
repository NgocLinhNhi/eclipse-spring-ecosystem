package heavensky.project.spring.boot.angularjs.serviceimpl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import heavensky.project.spring.boot.angularjs.entity.Product;
import heavensky.project.spring.boot.angularjs.repository.ProductRepository;
import heavensky.project.spring.boot.angularjs.service.ProductService;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    ProductRepository productRepository;

    @Override
    public List<Product> loadAllProduct() {
        List<Product> result = productRepository.loadAllProduct();
        result.forEach(product -> product.setCategoryName(product.getCategory().getCategoryName()));
        return result;
    }

    @Override
    public Product findProductById(Long seqPro) {
        Product result = productRepository.findProductById(seqPro);
        result.setCategoryId(result.getCategory().getCategoryId());
        result.setCategoryName(result.getCategory().getCategoryName());
        return result;
    }

    @Override
    public Boolean addNewProduct(Product pro) {
        Product saveAndFlush = productRepository.saveAndFlush(createProduct(pro));
        return saveAndFlush != null;
    }

    @Override
    public long selectMaxSeqProduct() {
        return productRepository.selectMaxSeqProduct();
    }

    @Override
    public void deleteProduct(Long seqPro) {
        productRepository.deleteById(seqPro);
    }

    @Override
    public void updateProduct(Product pro) {
        productRepository.save(pro);
    }

    @Override
    public Boolean changeStatus(Long seqPro) {
        int result = productRepository.updateStatusProduct(seqPro);
        return result != 0;
    }

    private Product createProduct(Product product) {
        long selectMaxSeqProduct = productRepository.selectMaxSeqProduct();
        long seqNo = selectMaxSeqProduct + 1;

        product.setSeqPro(seqNo);
        product.setSysStatus(new Long("1"));

        Date date = new Date();
        product.setCreateDate(date);
        product.setUpdateDate(date);
        return product;
    }

}
