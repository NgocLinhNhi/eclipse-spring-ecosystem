package heavensky.project.spring.boot.angularjs.service;

import java.util.List;

import heavensky.project.spring.boot.angularjs.entity.Product;

public interface ProductService {
    List<Product> loadAllProduct();

    Product findProductById(Long seqPro);

    Boolean addNewProduct(Product pro);

    long selectMaxSeqProduct();

    void deleteProduct(Long seqPro) throws Exception;

    Boolean changeStatus(Long seqPro) throws Exception;

    void updateProduct(Product pro) throws Exception;
}
