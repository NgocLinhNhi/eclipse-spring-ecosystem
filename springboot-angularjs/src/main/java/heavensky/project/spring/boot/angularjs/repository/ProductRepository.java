package heavensky.project.spring.boot.angularjs.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import heavensky.project.spring.boot.angularjs.entity.Product;
import heavensky.project.spring.boot.angularjs.sql.SqlQuery;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

	@Query(SqlQuery.LOAD_ALL_PRODUCT)
	List<Product> loadAllProduct();

	@Query(SqlQuery.FIND_PRODUCT_BY_ID)
	Product findProductById(@Param("seqPro") Long seqPro);

	@Query(value = SqlQuery.SELECT_MAX_SEQ, nativeQuery = true)
	long selectMaxSeqProduct();

	@Transactional
	@Modifying(clearAutomatically = true)
	@Query(SqlQuery.UPDATE_PRODUCT)
	int updateStatusProduct(Long id);
}
