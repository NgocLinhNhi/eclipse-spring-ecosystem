package heavensky.project.spring.boot.angularjs.service;

import heavensky.project.spring.boot.angularjs.entity.User;

public interface UserService {
    User loginUser(User us);
}
