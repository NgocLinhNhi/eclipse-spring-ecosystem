package heavensky.project.spring.boot.angularjs.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(name = "DIARYADMIN")
public class Diary implements Serializable {

	private static final long serialVersionUID = -731200869916708312L;

	@Id
	@Column(name = "SEQ_NO")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int seqNo;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ADMIN_ID", nullable = false)
	@Fetch(FetchMode.SELECT)
	private Admin admin;

	@Temporal(TemporalType.DATE)
	@Column(name = "CREATE_DATE")
	private Date createDate;

	@Column(name = "CONTENT_DIARY")
	private String contentDiary;

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Diary(int seqNo, Admin admin, Date createDate, String contentDiary) {
		super();
		this.seqNo = seqNo;
		this.admin = admin;
		this.createDate = createDate;
		this.contentDiary = contentDiary;
	}

	public Diary() {
		super();
	}

	public int getSeqNo() {
		return seqNo;
	}

	public void setSeqNo(int seqNo) {
		this.seqNo = seqNo;
	}

	public Admin getAdmin() {
		return admin;
	}

	public void setAdmin(Admin admin) {
		this.admin = admin;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getContentDiary() {
		return contentDiary;
	}

	public void setContentDiary(String contentDiary) {
		this.contentDiary = contentDiary;
	}
	

}
