package heavensky.project.spring.boot.angularjs.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(name = "USER_CUSTOMER")
public class User implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "SEQ_USER", nullable = false)
	private Long seqUser;

	@Column(name = "LOGIN_NAME", nullable = false)
	private String loginName;

	@Column(name = "PASSWORD", nullable = false)
	private String password;

	@Column(name = "EMAIL")
	private String email;

	@Column(name = "PHONE")
	private String phone;

	@Column(name = "ADDRESS")
	private String address;

	@Column(name = "GENDER")
	private int gender;

	@Column(name = "USER_NAME")
	private String userName;

	@Column(name = "ROLE_USER")
	private int roleUser;

	@Column(name = "IS_DELETE")
	private int isDelete;

	@Column(name = "IDENTITY_CARD", length = 9)
	private String identityCard;

	@Temporal(TemporalType.DATE)
	@Column(name = "CREATE_DATE")
	private Date createDate;

	@Column(name = "LINK_BILL")
	private String linkBill;

	@OneToMany(mappedBy = "customer", fetch = FetchType.LAZY)
	@Fetch(FetchMode.SELECT)
	private Set<IdeaCustomer> ideacustomers;

	@OneToMany(mappedBy = "customer", fetch = FetchType.LAZY)
	@Fetch(FetchMode.SELECT)
	private Set<Bill> bills;

	public User(Long seqUser, String loginName, String password, String email, String phone, String address, int gender,
			String userName, int roleUser, int isDelete, String identityCard, Date createDate, String linkBill,
			Set<IdeaCustomer> ideacustomers, Set<Bill> bills) {
		super();
		this.seqUser = seqUser;
		this.loginName = loginName;
		this.password = password;
		this.email = email;
		this.phone = phone;
		this.address = address;
		this.gender = gender;
		this.userName = userName;
		this.roleUser = roleUser;
		this.isDelete = isDelete;
		this.identityCard = identityCard;
		this.createDate = createDate;
		this.linkBill = linkBill;
		this.ideacustomers = ideacustomers;
		this.bills = bills;
	}

	public User() {
		super();
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Long getSeqUser() {
		return seqUser;
	}

	public void setSeqUser(Long seqUser) {
		this.seqUser = seqUser;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getGender() {
		return gender;
	}

	public void setGender(int gender) {
		this.gender = gender;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public int getRoleUser() {
		return roleUser;
	}

	public void setRoleUser(int roleUser) {
		this.roleUser = roleUser;
	}

	public int getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(int isDelete) {
		this.isDelete = isDelete;
	}

	public String getIdentityCard() {
		return identityCard;
	}

	public void setIdentityCard(String identityCard) {
		this.identityCard = identityCard;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getLinkBill() {
		return linkBill;
	}

	public void setLinkBill(String linkBill) {
		this.linkBill = linkBill;
	}

	public Set<IdeaCustomer> getIdeacustomers() {
		return ideacustomers;
	}

	public void setIdeacustomers(Set<IdeaCustomer> ideacustomers) {
		this.ideacustomers = ideacustomers;
	}

	public Set<Bill> getBills() {
		return bills;
	}

	public void setBills(Set<Bill> bills) {
		this.bills = bills;
	}
	
}
