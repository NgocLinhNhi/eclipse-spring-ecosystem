package heavensky.project.spring.boot.angularjs.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import heavensky.project.spring.boot.angularjs.entity.User;
import heavensky.project.spring.boot.angularjs.repository.UserRepository;
import heavensky.project.spring.boot.angularjs.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	UserRepository userRepository;

	@Override
	public User loginUser(User us) {
		return userRepository.loginUser(us.getLoginName(), us.getPassword());
	}

}