package heavensky.project.spring.boot.angularjs.sql;

public class SqlQuery {
    public static final String LOAD_ALL_PRODUCT = "Select pro From Product pro";
    public static final String FIND_PRODUCT_BY_ID = " Select pro from Product pro where pro.seqPro=:seqPro";
    public static final String SELECT_MAX_SEQ = "select max(SEQ_PRO) from PRODUCT";
    public static final String UPDATE_PRODUCT = "update Product pro set pro.sysStatus = 0 where pro.seqPro = ?1";

    public static final String LOGIN = "select us from User us where us.loginName = ?1 and us.password = ?2 and isDelete=1";

}
