package heavensky.project.spring.boot.angularjs.utils;

public class Constant {
    public static final String EXCEPTION = "Exception";

    public static class RESPONSE {
        public static final int SUCCESS_STATUS = 200;
        public static final String SUCCESS_CODE = "SUCCESS";
        public static final String SUCCESS_MESSAGE = "Thực hiện thành công!";

        public static final int EXCEPTION_STATUS = 0;
        public static final String EXCEPTION_CODE = "EXCEPTION";
        public static final String EXCEPTION_MESSAGE = "Thực hiện không thành công!";

        public static final String EXIST_MESSAGE = "Dữ liệu đã được sử dụng, không được xóa";
        public static final String EXIST_USERNAME = "Tên đăng nhập đã tồn tại";

        public static final String EXIST_USER_NAME = "Tên đăng nhập đã tồn tại!";
    }
}
