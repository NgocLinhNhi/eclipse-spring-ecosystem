package heavensky.project.spring.boot.angularjs.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import heavensky.project.spring.boot.angularjs.entity.User;
import heavensky.project.spring.boot.angularjs.sql.SqlQuery;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    @Query(SqlQuery.LOGIN)
    User loginUser(String loginName, String password);

}
