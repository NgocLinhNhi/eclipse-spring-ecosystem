package heavensky.project.spring.boot.angularjs.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import heavensky.project.spring.boot.angularjs.entity.User;
import heavensky.project.spring.boot.angularjs.message.Response;
import heavensky.project.spring.boot.angularjs.serviceimpl.UserServiceImpl;
import heavensky.project.spring.boot.angularjs.utils.Constant;

@RestController
@RequestMapping("/user")
public class UserController {

	@Autowired
	UserServiceImpl userService;

	@PostMapping(value = "/login")
	public ResponseEntity<Response> showWelcomePage(@RequestParam String loginName, @RequestParam String password) {

		User us = new User();
		us.setLoginName(loginName);
		us.setPassword(password);

		User loginUser = userService.loginUser(us);

		if (loginUser == null) {
			return new ResponseEntity<>(new Response(
					Constant.RESPONSE.EXCEPTION_STATUS,
					Constant.RESPONSE.EXCEPTION_CODE, 
					Constant.RESPONSE.EXCEPTION_MESSAGE),
					HttpStatus.EXPECTATION_FAILED);
		}
		return new ResponseEntity<>(new Response(
				Constant.RESPONSE.SUCCESS_STATUS, 
				Constant.RESPONSE.SUCCESS_CODE,
				Constant.RESPONSE.SUCCESS_MESSAGE),
				HttpStatus.OK);
	}
}
