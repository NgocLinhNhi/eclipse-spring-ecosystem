package heavensky.project.spring.boot.angularjs.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "CATEGORY")
public class Category implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "CATEGORY_ID", nullable = false)
	private Long categoryId;

	// @JsonBackReference
	@ManyToOne
	@JsonIgnore
	@JoinColumn(name = "PRODUCER_ID")
	private Producer producer;

	@Column(name = "CATEGORY_NAME")
	private String categoryName;

	@Column(name = "SYS_STATUS_CATE")
	private int sysStatusCate;

	@OneToMany(mappedBy = "category")
	@JsonManagedReference //cach khac de ko dung JsonIgrnore @JsonManagedReference+@JsonBackReference
	private Set<Product> product;

	public Category(Long categoryId, Producer producer, String categoryName, int sysStatusCate, Set<Product> product) {
		super();
		this.categoryId = categoryId;
		this.producer = producer;
		this.categoryName = categoryName;
		this.sysStatusCate = sysStatusCate;
		this.product = product;
	}

	public Category() {
		super();
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public Producer getProducer() {
		return producer;
	}

	public void setProducer(Producer producer) {
		this.producer = producer;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public int getSysStatusCate() {
		return sysStatusCate;
	}

	public void setSysStatusCate(int sysStatusCate) {
		this.sysStatusCate = sysStatusCate;
	}

	public Set<Product> getProduct() {
		return product;
	}

	public void setProduct(Set<Product> product) {
		this.product = product;
	}

}
