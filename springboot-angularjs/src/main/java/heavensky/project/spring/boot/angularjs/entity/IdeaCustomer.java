package heavensky.project.spring.boot.angularjs.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(name = "IDEACUSTOMER")
public class IdeaCustomer implements Serializable {
	private static final long serialVersionUID = -593152082331964862L;

	@Id
	@Column(name = "SEQ_NO")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int seqNo;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CUSTOMER_ID")
	@Fetch(FetchMode.SELECT)
	private User customer;

	@Column(name = "PRODUCT_ID")
	private Integer productId;

	@Temporal(TemporalType.DATE)
	@Column(name = "CREATE_DATE")
	private Date createDate;

	@Column(name = "TITLE")
	private String title;

	@Column(name = "CONTENT_IDEA")
	private String contentIdea;

	@Column(name = "SYS_STATUS")
	private int sysStatus;

	public IdeaCustomer(int seqNo, User customer, Integer productId, Date createDate, String title, String contentIdea,
			int sysStatus) {
		super();
		this.seqNo = seqNo;
		this.customer = customer;
		this.productId = productId;
		this.createDate = createDate;
		this.title = title;
		this.contentIdea = contentIdea;
		this.sysStatus = sysStatus;
	}

	public IdeaCustomer() {
		super();
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public int getSeqNo() {
		return seqNo;
	}

	public void setSeqNo(int seqNo) {
		this.seqNo = seqNo;
	}

	public User getCustomer() {
		return customer;
	}

	public void setCustomer(User customer) {
		this.customer = customer;
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContentIdea() {
		return contentIdea;
	}

	public void setContentIdea(String contentIdea) {
		this.contentIdea = contentIdea;
	}

	public int getSysStatus() {
		return sysStatus;
	}

	public void setSysStatus(int sysStatus) {
		this.sysStatus = sysStatus;
	}

}