package spring.boot.security.jsp.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private UserDetailsService userDetailsService;

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		// login voi jsp phai dung http.csrf nay disabled no di
		http.csrf().disable();
		// Các trang không yêu cầu login
		http.authorizeRequests().antMatchers("/", "/welcome", "/login", "/logout").permitAll();

		http.authorizeRequests().antMatchers("/index").hasAnyRole("MEMBER","ADMIN");
		http.authorizeRequests().antMatchers("/admin").hasRole("ADMIN");
		http.authorizeRequests().and().exceptionHandling().accessDeniedPage("/403");
		//có thể tạo ra 1 URI 403 của controller để set message access denied
		
		// Cấu hình cho Login Form.
		http.authorizeRequests().and().formLogin()
				// Submit URL của trang login
				//cái này ko cần định nghĩa trong controller chỉ cần định nghĩa action ở form login
				.loginProcessingUrl("/check_login_security") 
				.loginPage("/login")//
				.defaultSuccessUrl("/index")//
				.failureUrl("/?error=true")//login fail trả về trang login
				.usernameParameter("email")//
				.passwordParameter("password")

				// Cấu hình cho Logout Page.
				.and()
				.logout()
				//cái này ko cần định nghĩa trong controller chỉ cần định nghĩa action ở form logout
				.logoutUrl("/logout")
				//trỏ đến URI trong controller
				.logoutSuccessUrl("/logoutSuccessful")
				.deleteCookies("auth_code", "JSESSIONID").invalidateHttpSession(true);
	}

}
