package spring.boot.security.jsp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import spring.boot.security.jsp.entity.Role;

public interface RoleRepository extends JpaRepository<Role, Integer> {

	Role findByName(String name);

}
