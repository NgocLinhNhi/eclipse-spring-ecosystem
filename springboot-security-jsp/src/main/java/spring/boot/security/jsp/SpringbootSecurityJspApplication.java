package spring.boot.security.jsp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootSecurityJspApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootSecurityJspApplication.class, args);
	}

}
