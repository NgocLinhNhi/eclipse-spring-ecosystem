package spring.boot.security.jsp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import spring.boot.security.jsp.entity.User;

public interface UserRepository extends JpaRepository<User, Integer> {

	User findByEmail(String email);

}
