<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>

<head>
<title>Spring Security Example</title>
<link rel="stylesheet" type="text/css" href="/bootstrap/js/bootstrap.min.css">
<script src="/jquerry/jquery.min.js"></script>
<script src="/bootstrap/js/bootstrap.min.js"></script>
<link href="/css/style.css" type="text/css" rel="stylesheet" />
</head>

<body>
	<div class="container" style="margin: 50px">
		<h3>Spring Security Login Example</h3>
		<c:if test="${param.error}"><p class="error">Wrong userName or password</p></c:if>

		<!-- check_login_security chỉ cần định nghĩa trong file webSecurityConfig.java ko cần định nghĩa ở controller -->
		<form name ="formLogin "action="/check_login_security" method="POST">
			<div class="form-group">
				<label for="email">UserName: <input type="text" class="form-control" id="email" name="email"></label>		
			</div>
			
			<div class="form-group">
				<label for="pwd">Password:</label> <input type="password" style="width: 220px;" class="form-control" id="pwd" name="password">		
			</div>

			<button type="submit" class="btn btn-success">Submit</button>

		</form>
	</div>
</body>
</html>