package heavensky.spring.boot.shop.online.validator;

import heavensky.spring.boot.shop.online.exception.ExcelValidateException;
import heavensky.spring.boot.shop.online.message.GatewayInvalidField;
import heavensky.spring.boot.shop.online.constant.HttpResponseErrors;
import heavensky.spring.boot.shop.online.utils.StringUtil;
import heavensky.spring.boot.shop.online.view.ProductFileExcel;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ExcelValidator {
    private static Logger logger = Logger.getLogger(ExcelValidator.class);

    public void validate(List<ProductFileExcel> products) throws ExcelValidateException {
        List<GatewayInvalidField> errors = new ArrayList<>();

        validateFileSize(errors, products);
        validateProductNameField(errors, products);
        validateCategoryField(errors, products);

        if (errors.size() > 0) {
            throw new ExcelValidateException(errors);
        }
    }

    private void validateFileSize(List<GatewayInvalidField> errors, List<ProductFileExcel> products) {
        if (products.size() > 10) {
            logger.error("File Dont Allow more than 10 Row !!!");
            errors.add(new GatewayInvalidField("pageSize", HttpResponseErrors.OVER_LENGTH));
        }
    }

    private void validateProductNameField(List<GatewayInvalidField> errors, List<ProductFileExcel> products) {
        for (ProductFileExcel pro : products) {
            if (StringUtil.isEmpty(pro.getProductName())) {
                errors.add(new GatewayInvalidField("productName", HttpResponseErrors.REQUIRED));
            }
        }
    }

    private void validateCategoryField(List<GatewayInvalidField> errors, List<ProductFileExcel> products) {
        for (ProductFileExcel pro : products) {
            if (pro.getCategory() == null) {
                errors.add(new GatewayInvalidField("category", HttpResponseErrors.REQUIRED));
            }
        }
    }
}
