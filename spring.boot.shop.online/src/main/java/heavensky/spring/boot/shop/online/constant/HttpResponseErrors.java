package heavensky.spring.boot.shop.online.constant;

public final class HttpResponseErrors {

    public static final String REQUIRED = "required";
    public static final String OVER_LENGTH = "over_length";
    public static final String INVALID = "invalid";
    public static final String INVALID_STATUS = "invalidStatus";
    public static final String INVALID_BALANCE = "invalidBalance";
    public static final String EXIST_POSITION = "existPosition";
    public static final String INSUFFICIENT_FUNDS = "insufficientFunds";

    private HttpResponseErrors() {}

}
