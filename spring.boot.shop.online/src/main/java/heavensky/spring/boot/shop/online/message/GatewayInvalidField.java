package heavensky.spring.boot.shop.online.message;

public class GatewayInvalidField {

    protected final String field;
    protected final String error;
    
	public GatewayInvalidField(String field, String error) {
		super();
		this.field = field;
		this.error = error;
	}
	
	public String getField() {
		return field;
	}
	public String getError() {
		return error;
	}
	
	
    
}
