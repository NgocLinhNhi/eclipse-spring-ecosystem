package heavensky.spring.boot.shop.online.services;

import heavensky.spring.boot.shop.online.entity.Category;

import java.util.List;

public interface CategoryService {
    List<Category> loadAllCategory();
}
