package heavensky.spring.boot.shop.online.services;

import heavensky.spring.boot.shop.online.entity.Product;

import java.util.List;

public interface ReportService {
    /**
     * Lấy data từ Repository bình thường
     */
    List<Product> reportListProduct();

    /**
     * Lấy data từ Stored
     */
    List<Product> exportExcelReportByStoreProcedure();

    /**
     * Lấy data từ View
     */
    List<Product> reportListProductView();
}
