package heavensky.spring.boot.shop.online.services_impl;

import heavensky.spring.boot.shop.online.entity.Category;
import heavensky.spring.boot.shop.online.entity.Product;
import heavensky.spring.boot.shop.online.exception.GatewayNotFoundException;
import heavensky.spring.boot.shop.online.repository.ProductRepository;
import heavensky.spring.boot.shop.online.services.ProductService;
import heavensky.spring.boot.shop.online.view.ProductFileExcel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    ProductRepository productRepository;

    @Override
    public List<Product> loadAllProduct() {
        List<Product> result = productRepository.loadAllProduct();
        for (Product product : result) {
            product.setCategoryName(product.getCategory().getCategoryName());
        }
        return result;
    }

    @Override
    public Product findProductById(Long seqPro) {
        Product result = productRepository.findProductById(seqPro);
        if (result == null) {
            throw new GatewayNotFoundException("Product not found ");
        }
        result.setCategoryId(result.getCategory().getCategoryId());
        result.setCategoryName(result.getCategory().getCategoryName());
        return result;
    }

    @Override
    public Boolean addNewProduct(Product pro) {
        createProduct(pro);
        Product saveAndFlush = productRepository.saveAndFlush(pro);
        return saveAndFlush != null;
    }

    private void createProduct(Product pro) {
        long selectMaxSeqProduct = productRepository.selectMaxSeqProduct();
        long seqNo = selectMaxSeqProduct + 1;

        pro.setSeqPro(seqNo);
        pro.setSysStatus(new Long("1"));
        pro.setIsUsing("N");

        Date date = new Date();
        pro.setCreateDate(date);
        pro.setUpdateDate(date);
    }

    @Override
    public long selectMaxSeqProduct() {
        return productRepository.selectMaxSeqProduct();
    }

    @Override
    public void deleteProduct(Long seqPro) {
        productRepository.deleteById(seqPro);
    }

    @Override
    public void updateProduct(Product pro) {
        productRepository.save(pro);
    }

    @Override
    public Boolean changeStatus(Long seqPro) {
        int result = productRepository.updateStatusProduct(seqPro);
        return result != 0;

    }

    @Override
    public void addListProductBatch(List<ProductFileExcel> listProduct) {
        List<Product> products = createProducts(listProduct);
        productRepository.saveAll(products);
    }

    private List<Product> createProducts(List<ProductFileExcel> listProduct) {
        List<Product> listProducts = new ArrayList<>();

        long seqNo = getMaxSeqPro();
        Product product;

        for (ProductFileExcel pro : listProduct) {
            product = new Product();
            Category cate = new Category();

            product.setSeqPro(seqNo);
            cate.setCategoryId(pro.getCategory());
            product.setCategory(cate);
            product.setProductName(pro.getProductName());

            listProducts.add(product);
            seqNo++;
        }
        return listProducts;
    }

    private long getMaxSeqPro() {
        long selectMaxSeqProduct = productRepository.selectMaxSeqProduct();
        return selectMaxSeqProduct + 1;
    }

}
