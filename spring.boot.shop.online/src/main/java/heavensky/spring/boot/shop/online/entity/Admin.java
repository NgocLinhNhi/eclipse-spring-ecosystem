package heavensky.spring.boot.shop.online.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(name = "ADMIN")
public class Admin implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "SEQ_ADMIN", nullable = false)
	private Long seqUser;

	@Column(name = "LOGIN_ADMIN", nullable = false)
	private String loginName;

	@Column(name = "PASSWORD_ADMIN", nullable = false)
	private String password;

	@Column(name = "EMAIL_ADMIN")
	private String email;

	@Column(name = "ADDRESS_ADMIN")
	private String address;

	@Temporal(TemporalType.DATE)
	@Column(name = "DATE_OF_BIRTH")
	private Date dateOfBirth;

	@Column(name = "GENDER")
	private int gender;

	@Column(name = "SYS_STATUS")
	private int sysStatus;

	@OneToMany(mappedBy = "admin", fetch = FetchType.LAZY)
	@Fetch(FetchMode.SELECT)
	private Set<Diary> diaryAdmin;

	public Admin(Long seqUser, String loginName, String password, String email, String address, Date dateOfBirth,
			int gender, int sysStatus, Set<Diary> diaryAdmin) {
		super();
		this.seqUser = seqUser;
		this.loginName = loginName;
		this.password = password;
		this.email = email;
		this.address = address;
		this.dateOfBirth = dateOfBirth;
		this.gender = gender;
		this.sysStatus = sysStatus;
		this.diaryAdmin = diaryAdmin;
	}

	public Admin() {
		super();
	}

	public Long getSeqUser() {
		return seqUser;
	}

	public void setSeqUser(Long seqUser) {
		this.seqUser = seqUser;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public int getGender() {
		return gender;
	}

	public void setGender(int gender) {
		this.gender = gender;
	}

	public int getSysStatus() {
		return sysStatus;
	}

	public void setSysStatus(int sysStatus) {
		this.sysStatus = sysStatus;
	}

	public Set<Diary> getDiaryAdmin() {
		return diaryAdmin;
	}

	public void setDiaryAdmin(Set<Diary> diaryAdmin) {
		this.diaryAdmin = diaryAdmin;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
