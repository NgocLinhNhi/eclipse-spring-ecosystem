package heavensky.spring.boot.shop.online.sql;

public class ProductSqlQuery {
    public static final String LOAD_ALL_PRODUCT = "Select pro From Product pro";
    public static final String LOAD_PRODUCT_BY_VIEW = "Select pro From ProductView pro";
    public static final String FIND_PRODUCT_BY_ID = "Select pro from Product pro where pro.seqPro=:seqPro";
    public static final String MAX_SEQ_PRODUCT = "select max(SEQ_PRO) from PRODUCT";
    public static final String UPDATE_PRODUCT = "update Product pro set pro.sysStatus = 0 where pro.seqPro = ?1";
}
