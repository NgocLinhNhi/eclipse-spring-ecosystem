package heavensky.spring.boot.shop.online.exception;

public class ExcelValidateException extends Exception {
    private Object data;

    public ExcelValidateException(String message) {
        super(message);
    }

    public ExcelValidateException(Object data) {
        this("bad request", data);
    }

    public ExcelValidateException(String message, Object data) {
        super(message + ", data: " + data);
        this.data = data;
    }

    public ExcelValidateException(String message, Object data, Exception e) {
        super(message + ", data: " + data, e);
        this.data = data;
    }
}


