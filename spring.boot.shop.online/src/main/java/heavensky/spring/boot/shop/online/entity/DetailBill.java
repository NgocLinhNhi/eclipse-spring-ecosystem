package heavensky.spring.boot.shop.online.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(name = "DETAILBILL")
public class DetailBill implements Serializable {

	private static final long serialVersionUID = 3952535318569442430L;

	@EmbeddedId
	private DetailBillPK seq_no;

	@ManyToOne(fetch = FetchType.LAZY)
	// ko set Insertable va update able = false thi chet ngay loi nay :
	// Initial SessionFactory creation failed.org.hibernate.MappingException:
	// Repeated column in mapping
	// for entity: com.jp.salesonline.entity.DetailBill column: BILL_ID
	@JoinColumn(name = "BILL_ID", nullable = false, insertable = false, updatable = false)
	@Fetch(FetchMode.SELECT)
	private Bill bill;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PRODUCT_ID", nullable = false, insertable = false, updatable = false)
	@Fetch(FetchMode.SELECT)
	private Product product;

	@Column(name = "NUMBER_BILL")
	private Integer numberBill;

	@Column(name = "SINGLE_PRICE")
	private BigDecimal singlePrice;

	@Column(name = "SYS_STATUS_DETAILBILL")
	private int sysStatusDetailBill;

	public DetailBill(DetailBillPK seq_no, Bill bill, Product product, Integer numberBill, BigDecimal singlePrice,
			int sysStatusDetailBill) {
		super();
		this.seq_no = seq_no;
		this.bill = bill;
		this.product = product;
		this.numberBill = numberBill;
		this.singlePrice = singlePrice;
		this.sysStatusDetailBill = sysStatusDetailBill;
	}

	public DetailBill() {
		super();
	}

	public DetailBillPK getSeq_no() {
		return seq_no;
	}

	public void setSeq_no(DetailBillPK seq_no) {
		this.seq_no = seq_no;
	}

	public Bill getBill() {
		return bill;
	}

	public void setBill(Bill bill) {
		this.bill = bill;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Integer getNumberBill() {
		return numberBill;
	}

	public void setNumberBill(Integer numberBill) {
		this.numberBill = numberBill;
	}

	public BigDecimal getSinglePrice() {
		return singlePrice;
	}

	public void setSinglePrice(BigDecimal singlePrice) {
		this.singlePrice = singlePrice;
	}

	public int getSysStatusDetailBill() {
		return sysStatusDetailBill;
	}

	public void setSysStatusDetailBill(int sysStatusDetailBill) {
		this.sysStatusDetailBill = sysStatusDetailBill;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
