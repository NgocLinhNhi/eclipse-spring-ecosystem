package heavensky.spring.boot.shop.online.sql;

public class UserSqlQuery {
    public static final String LOGIN = "select us from User us where us.loginName = ?1 and us.password = ?2 and isDelete=1";
    public static final String LOGIN_TYPE_2 = "select us.* from USER_CUSTOMER us where us.LOGIN_NAME = ?1 and us.PASSWORD = ?2";
    public static final String SELECT_MAX_SEQ = "select max(SEQ_USER) from USER_CUSTOMER";
    public static final String CHECK_EXIST_USER_NAME = "select us from User us where us.loginName=:loginName and us.isDelete=1";
}
