package heavensky.spring.boot.shop.online;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootShopOnlineApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootShopOnlineApplication.class, args);
	}

}
