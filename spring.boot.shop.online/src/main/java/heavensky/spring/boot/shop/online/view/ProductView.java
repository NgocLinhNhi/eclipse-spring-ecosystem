package heavensky.spring.boot.shop.online.view;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
// tên view trong DB
// sau đó chỉ việc select all Data từ view là xong các dk join đã được thiết kế sẵn trong view DB
@Table(name = "VIEW_PRODUCT")
public class ProductView implements Serializable {

	private static final long serialVersionUID = -5538416125326898258L;

	@Id
	@Column(name = "SEQ_PRO")
	private Long seqPro;

	@Column(name = "CATEGORY_ID", nullable = false)
	private String categoryId;

	@Column(name = "PRODUCT_NAME")
	private String productName;

	@Column(name = "PRICE")
	private BigDecimal price;

	@Column(name = "IMAGE_URL")
	private String imageProduct;

	@Column(name = "NUMBER_SALES")
	private BigDecimal numberSales;

	@Column(name = "GUARANTEE")
	private BigDecimal guarantee;

	@Temporal(TemporalType.DATE)
	@Column(name = "CREATE_DATE")
	private Date createDate;

	@Temporal(TemporalType.DATE)
	@Column(name = "UPDATE_DATE")
	private Date updateDate;

	@Column(name = "SYS_STATUS")
	private Long sysStatus;

	public ProductView() {
		super();
	}

	public ProductView(Long seqPro, String categoryId, String productName, BigDecimal price, String imageProduct,
			BigDecimal numberSales, BigDecimal guarantee, Date createDate, Date updateDate, Long sysStatus) {
		super();
		this.seqPro = seqPro;
		this.categoryId = categoryId;
		this.productName = productName;
		this.price = price;
		this.imageProduct = imageProduct;
		this.numberSales = numberSales;
		this.guarantee = guarantee;
		this.createDate = createDate;
		this.updateDate = updateDate;
		this.sysStatus = sysStatus;
	}

	public Long getSeqPro() {
		return seqPro;
	}

	public void setSeqPro(Long seqPro) {
		this.seqPro = seqPro;
	}

	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public String getImageProduct() {
		return imageProduct;
	}

	public void setImageProduct(String imageProduct) {
		this.imageProduct = imageProduct;
	}

	public BigDecimal getNumberSales() {
		return numberSales;
	}

	public void setNumberSales(BigDecimal numberSales) {
		this.numberSales = numberSales;
	}

	public BigDecimal getGuarantee() {
		return guarantee;
	}

	public void setGuarantee(BigDecimal guarantee) {
		this.guarantee = guarantee;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public Long getSysStatus() {
		return sysStatus;
	}

	public void setSysStatus(Long sysStatus) {
		this.sysStatus = sysStatus;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}