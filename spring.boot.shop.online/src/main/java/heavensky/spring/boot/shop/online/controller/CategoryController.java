package heavensky.spring.boot.shop.online.controller;

import heavensky.spring.boot.shop.online.entity.Category;
import heavensky.spring.boot.shop.online.services.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/cate")
public class CategoryController {

    @Autowired
    CategoryService categoryService;

    @GetMapping(value = "/loadAllCategory")
    public List<Category> loadAllCategory() {
        return categoryService.loadAllCategory();
    }
}
