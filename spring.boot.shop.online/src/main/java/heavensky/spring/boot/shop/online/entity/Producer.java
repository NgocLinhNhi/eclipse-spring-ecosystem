package heavensky.spring.boot.shop.online.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(name = "PRODUCER")
public class Producer implements Serializable {

	private static final long serialVersionUID = -4988499961184910623L;

	@Id
	@Column(name = "SEQ_NO")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int seqNo;

	@Column(name = "PRODUCER_NAME")
	private String producerName;

	@Column(name = "SYS_STATUS")
	private int sysStatus;

	@OneToMany(mappedBy = "producer", fetch = FetchType.LAZY)
	@Fetch(FetchMode.SELECT)
	private Set<Category> category;

	public Producer() {
		super();
	}

	public Producer(int seqNo, String producerName, int sysStatus, Set<Category> category) {
		super();
		this.seqNo = seqNo;
		this.producerName = producerName;
		this.sysStatus = sysStatus;
		this.category = category;
	}

	public int getSeqNo() {
		return seqNo;
	}

	public void setSeqNo(int seqNo) {
		this.seqNo = seqNo;
	}

	public String getProducerName() {
		return producerName;
	}

	public void setProducerName(String producerName) {
		this.producerName = producerName;
	}

	public int getSysStatus() {
		return sysStatus;
	}

	public void setSysStatus(int sysStatus) {
		this.sysStatus = sysStatus;
	}

	public Set<Category> getCategory() {
		return category;
	}

	public void setCategory(Set<Category> category) {
		this.category = category;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
