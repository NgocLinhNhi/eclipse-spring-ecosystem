package heavensky.spring.boot.shop.online.repository;

import heavensky.spring.boot.shop.online.entity.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

import static heavensky.spring.boot.shop.online.sql.CategorySqlQuery.GET_ALL_CATEGORY;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {

    @Query(GET_ALL_CATEGORY)
    List<Category> loadAllCategory();
}
