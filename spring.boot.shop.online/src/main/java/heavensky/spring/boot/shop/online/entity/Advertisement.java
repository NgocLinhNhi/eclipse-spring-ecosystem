package heavensky.spring.boot.shop.online.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "ADVERTISEMENT")
public class Advertisement implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "SEQ_ADV", nullable = false)
	private Long seqAdv;

	@Column(name = "ADMIN_ID", nullable = false)
	private Long adminId;

	@Column(name = "TITLE", nullable = false)
	private String title;

	@Column(name = "CONTENT_ADV")
	private String contentAdv;

	@Column(name = "VIDEO_URL")
	private String videoUrl;

	@Column(name = "IMAGE_URL")
	private String imageUrl;

	@Temporal(TemporalType.DATE)
	@Column(name = "START_DATE")
	private Date startDate;

	@Temporal(TemporalType.DATE)
	@Column(name = "END_DATE")
	private Date endDate;

	@Column(name = "SYS_STATUS")
	private int sysStatus;

	public Advertisement(Long seqAdv, Long adminId, String title, String contentAdv, String videoUrl, String imageUrl,
			Date startDate, Date endDate, int sysStatus) {
		super();
		this.seqAdv = seqAdv;
		this.adminId = adminId;
		this.title = title;
		this.contentAdv = contentAdv;
		this.videoUrl = videoUrl;
		this.imageUrl = imageUrl;
		this.startDate = startDate;
		this.endDate = endDate;
		this.sysStatus = sysStatus;
	}

	public Advertisement() {
		super();
	}

	public Long getSeqAdv() {
		return seqAdv;
	}

	public void setSeqAdv(Long seqAdv) {
		this.seqAdv = seqAdv;
	}

	public Long getAdminId() {
		return adminId;
	}

	public void setAdminId(Long adminId) {
		this.adminId = adminId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContentAdv() {
		return contentAdv;
	}

	public void setContentAdv(String contentAdv) {
		this.contentAdv = contentAdv;
	}

	public String getVideoUrl() {
		return videoUrl;
	}

	public void setVideoUrl(String videoUrl) {
		this.videoUrl = videoUrl;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public int getSysStatus() {
		return sysStatus;
	}

	public void setSysStatus(int sysStatus) {
		this.sysStatus = sysStatus;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
