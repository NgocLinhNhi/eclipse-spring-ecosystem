package heavensky.spring.boot.shop.online.controller;

import heavensky.spring.boot.shop.online.constant.Constant;
import heavensky.spring.boot.shop.online.excel_service.ExcelProcessHandler;
import heavensky.spring.boot.shop.online.excel_service.ImportExcelReportService;
import heavensky.spring.boot.shop.online.exception.ExcelValidateException;
import heavensky.spring.boot.shop.online.view.ProductFileExcel;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.List;

@Controller
@RequestMapping(value = "/report")
public class FileUploadController {
    private static Logger logger = Logger.getLogger(FileUploadController.class);

    @Autowired
    private ExcelProcessHandler excelProcessHandler;

    @Autowired
    private ImportExcelReportService importExcelReportService;

    @PostMapping("/importProductByFileExcel")
    public ResponseEntity<String> uploadExcelVpoint(HttpServletRequest request) {
        StringBuilder message = new StringBuilder();
        try {
            File file = importExcelReportService.resumeAbleUpload(request);
            if (file == null) return new ResponseEntity<>(Constant.RESPONSE.FILE_LOADING, HttpStatus.NOT_FOUND);
            List<ProductFileExcel> excelProductResult = excelProcessHandler.processExcelProduct(file);

            message.append(Constant.RESPONSE.FILE_SUCCESS)
                    .append(excelProductResult.size())
                    .append(Constant.RESPONSE.FILE_TOTAL_DATA);

        } catch (IOException | ServletException e) {
            logger.error("Handle file excel on server has error ", e);
            return new ResponseEntity<>(Constant.RESPONSE.FILE_ERROR, HttpStatus.NOT_FOUND);
        } catch (ExcelValidateException e) {
            logger.error("Validate file excel has error ", e);
            return new ResponseEntity<>(Constant.RESPONSE.FILE_EXCEL_VALIDATE_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (Exception e) {
            logger.error("Handle Import file excel has error ", e);
            return new ResponseEntity<>(Constant.RESPONSE.FILE_EXCEL_IMPORT_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(message.toString(), HttpStatus.OK);
    }
}
