package heavensky.spring.boot.shop.online.entity;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class DetailBillPK implements Serializable {

	private static final long serialVersionUID = -1796781518509008144L;

	//Tên cột trong bảng DetailBill
	@Basic(optional = false)
	@Column(name = "BILL_ID")
	private int billId;

	@Basic(optional = false)
	@Column(name = "PRODUCT_ID")
	private int productId;

	public DetailBillPK() {
		super();
	}

	public DetailBillPK(int billId, int productId) {
		super();
		this.billId = billId;
		this.productId = productId;
	}

	public int getBillId() {
		return billId;
	}

	public void setBillId(int billId) {
		this.billId = billId;
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof DetailBillPK))
			return false;
		DetailBillPK castOther = (DetailBillPK) other;

		return (this.getBillId() == castOther.getBillId()) && (this.getProductId() == castOther.getProductId());
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result + this.getBillId();
		result = 37 * result + this.getProductId();
		return result;
	}
}
