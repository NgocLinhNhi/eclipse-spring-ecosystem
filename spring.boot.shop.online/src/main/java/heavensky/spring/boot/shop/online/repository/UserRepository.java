package heavensky.spring.boot.shop.online.repository;

import heavensky.spring.boot.shop.online.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import static heavensky.spring.boot.shop.online.sql.UserSqlQuery.*;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    @Query(LOGIN)
    User loginUser(String loginName, String password);

    @Query(value = LOGIN_TYPE_2, nativeQuery = true)
    User loginUser2(String loginName, String password);

    @Query(value = SELECT_MAX_SEQ, nativeQuery = true)
    int selectMaxUser();

    @Query(CHECK_EXIST_USER_NAME)
    User checkExistLoginName(@Param("loginName") String loginName);

}
