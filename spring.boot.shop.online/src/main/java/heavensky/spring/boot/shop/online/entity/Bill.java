package heavensky.spring.boot.shop.online.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(name = "BILL")
public class Bill implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "SEQ_BILL", nullable = false)
	private Long seqBill;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CUSTOMER_ID", nullable = false)
	@Fetch(FetchMode.SELECT)
	private User customer;

	@Column(name = "PHONE_CONTACT", nullable = false)
	private String phoneContact;

	@Temporal(TemporalType.DATE)
	@Column(name = "ORDER_DATE")
	private Date orderDate;

	@Temporal(TemporalType.DATE)
	@Column(name = "ACCEPT_DATE")
	private Date acceptDate;

	@Column(name = "SYS_STATUS_BILL")
	private int sysStatusBill;

	@Column(name = "ADDRESS_CUSTOMER")
	private String addressCustomer;

	@Column(name = "NAME_CUSTOMER")
	private String nameCustomer;

	@Column(name = "TOTAL_MONEY")
	private BigDecimal totalMoney;

	@OneToMany(mappedBy = "bill", fetch = FetchType.LAZY)
	@Fetch(FetchMode.SELECT)
	private Set<DetailBill> detailBill;

	public Bill(Long seqBill, User customer, String phoneContact, Date orderDate, Date acceptDate, int sysStatusBill,
			String addressCustomer, String nameCustomer, BigDecimal totalMoney, Set<DetailBill> detailBill) {
		super();
		this.seqBill = seqBill;
		this.customer = customer;
		this.phoneContact = phoneContact;
		this.orderDate = orderDate;
		this.acceptDate = acceptDate;
		this.sysStatusBill = sysStatusBill;
		this.addressCustomer = addressCustomer;
		this.nameCustomer = nameCustomer;
		this.totalMoney = totalMoney;
		this.detailBill = detailBill;
	}

	public Bill() {
		super();
	}

	public Long getSeqBill() {
		return seqBill;
	}

	public void setSeqBill(Long seqBill) {
		this.seqBill = seqBill;
	}

	public User getCustomer() {
		return customer;
	}

	public void setCustomer(User customer) {
		this.customer = customer;
	}

	public String getPhoneContact() {
		return phoneContact;
	}

	public void setPhoneContact(String phoneContact) {
		this.phoneContact = phoneContact;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public Date getAcceptDate() {
		return acceptDate;
	}

	public void setAcceptDate(Date acceptDate) {
		this.acceptDate = acceptDate;
	}

	public int getSysStatusBill() {
		return sysStatusBill;
	}

	public void setSysStatusBill(int sysStatusBill) {
		this.sysStatusBill = sysStatusBill;
	}

	public String getAddressCustomer() {
		return addressCustomer;
	}

	public void setAddressCustomer(String addressCustomer) {
		this.addressCustomer = addressCustomer;
	}

	public String getNameCustomer() {
		return nameCustomer;
	}

	public void setNameCustomer(String nameCustomer) {
		this.nameCustomer = nameCustomer;
	}

	public BigDecimal getTotalMoney() {
		return totalMoney;
	}

	public void setTotalMoney(BigDecimal totalMoney) {
		this.totalMoney = totalMoney;
	}

	public Set<DetailBill> getDetailBill() {
		return detailBill;
	}

	public void setDetailBill(Set<DetailBill> detailBill) {
		this.detailBill = detailBill;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
