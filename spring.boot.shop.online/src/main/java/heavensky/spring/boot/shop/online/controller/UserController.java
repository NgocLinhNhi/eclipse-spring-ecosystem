package heavensky.spring.boot.shop.online.controller;

import heavensky.spring.boot.shop.online.constant.Constant;
import heavensky.spring.boot.shop.online.entity.User;
import heavensky.spring.boot.shop.online.message.Response;
import heavensky.spring.boot.shop.online.services.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@RestController
@RequestMapping("/user")
public class UserController {
    private static Logger logger = Logger.getLogger(ProductController.class);

    @Autowired
    UserService userService;

    @PostMapping(value = "/login")
    public ModelAndView showWelcomePage(ModelMap model,
                                        @RequestParam String username,
                                        @RequestParam String password) {
        ModelAndView mv;

        User us = new User();
        us.setLoginName(username);
        us.setPassword(password);

        User loginUser = userService.loginUser(us);

        if (loginUser == null) {
            model.put("errorMessage", "Wrong User Name or password!!!");
            mv = new ModelAndView("user/login");
            return mv;
        }
        return new ModelAndView("pages/product");
    }

    @PostMapping(value = "/register")
    public ResponseEntity<Response> registerMember(@RequestBody User user) {
        try {
            Boolean checkExistLoginName = userService.checkExistLoginName(user);
            if (checkExistLoginName) {
                Boolean result = userService.registerMember(user);
                if (!result) {
                    return new ResponseEntity<>(new Response(
                            Constant.RESPONSE.EXCEPTION_STATUS,
                            Constant.RESPONSE.EXCEPTION_CODE,
                            Constant.RESPONSE.EXCEPTION_MESSAGE),
                            HttpStatus.EXPECTATION_FAILED);
                }
            } else {
                return new ResponseEntity<>(new Response(
                        Constant.RESPONSE.EXCEPTION_STATUS,
                        Constant.RESPONSE.EXCEPTION_CODE,
                        Constant.RESPONSE.EXIST_USER_NAME),
                        HttpStatus.CONFLICT);
            }
        } catch (Exception e) {
            logger.error(Constant.EXCEPTION, e);
            return new ResponseEntity<>(new Response(
                    Constant.RESPONSE.EXCEPTION_STATUS,
                    Constant.RESPONSE.EXCEPTION_CODE,
                    Constant.RESPONSE.EXCEPTION_MESSAGE),
                    HttpStatus.EXPECTATION_FAILED);
        }

        return new ResponseEntity<>(new Response(
                Constant.RESPONSE.SUCCESS_STATUS,
                Constant.RESPONSE.SUCCESS_CODE,
                Constant.RESPONSE.SUCCESS_MESSAGE),
                HttpStatus.OK);

    }
}
