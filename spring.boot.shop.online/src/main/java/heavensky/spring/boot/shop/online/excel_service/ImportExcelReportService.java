package heavensky.spring.boot.shop.online.excel_service;

import heavensky.spring.boot.shop.online.excel_import_utils.CreateResumeAbleFile;
import heavensky.spring.boot.shop.online.excel_import_utils.ResumeAbleInfo;
import heavensky.spring.boot.shop.online.excel_import_utils.ResumeAbleInfoStorage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;

@Service
public class ImportExcelReportService {

    @Autowired
    CreateResumeAbleFile createResumeAbleFile;

    public File resumeAbleUpload(HttpServletRequest request) throws ServletException, IOException {
        ResumeAbleInfo info = createResumeAbleFile.createResumeAbleInfo(request);
        //RandomAccessFile (rw) => hỗ trợ tạo 1 file y chang tên file upload trên server (không có .xlsx) ghi liền
        RandomAccessFile raf = new RandomAccessFile(info.resumeAbleFilePath, "rw");
        raf.seek((info.resumeAbleChunkNumber - 1) * (long) info.resumeAbleChunkSize);

        writeContentFile(request, raf);
        info.uploadedChunks.add(new ResumeAbleInfo.ResumeAbleChunkNumber(info.resumeAbleChunkNumber));

        if (!info.checkIfUploadFinished()) return null;
        ResumeAbleInfoStorage.getInstance().remove(info);
        return new File(info.finalFilePath);
    }

    private void writeContentFile(HttpServletRequest request, RandomAccessFile raf) throws IOException {
        // ghi nội dung file import vào file RandomAccessFile đã tạo trên server
        InputStream is = request.getInputStream();
        long readed = 0;
        long content_length = request.getContentLength();
        byte[] bytes = new byte[1024 * 100];
        while (readed < content_length) {
            int r = is.read(bytes);
            if (r < 0) {
                break;
            }
            raf.write(bytes, 0, r);
            readed += r;
        }
        raf.close();
    }

}
