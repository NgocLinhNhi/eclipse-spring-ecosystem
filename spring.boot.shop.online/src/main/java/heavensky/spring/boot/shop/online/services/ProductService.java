package heavensky.spring.boot.shop.online.services;

import heavensky.spring.boot.shop.online.entity.Product;
import heavensky.spring.boot.shop.online.view.ProductFileExcel;

import java.util.List;

public interface ProductService {
    List<Product> loadAllProduct();

    Product findProductById(Long seqPro);

    Boolean addNewProduct(Product pro);

    long selectMaxSeqProduct();

    void deleteProduct(Long seqPro) throws Exception;

    Boolean changeStatus(Long seqPro) throws Exception;

    void updateProduct(Product pro) throws Exception;

    void addListProductBatch(List<ProductFileExcel> listProduct);
}
