package heavensky.spring.boot.shop.online.excel_service;

import heavensky.spring.boot.shop.online.services.ProductService;
import heavensky.spring.boot.shop.online.validator.ExcelValidator;
import heavensky.spring.boot.shop.online.view.ProductFileExcel;
import org.apache.log4j.Logger;
import org.jxls.reader.ReaderBuilder;
import org.jxls.reader.XLSReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static heavensky.spring.boot.shop.online.constant.Constant.FILENAME.PRODUCT_EXCEL_IMPORT_TEMPLATE;

@Service
public class ExcelProcessHandler {
    private static Logger logger = Logger.getLogger(ExcelProcessHandler.class);

    @Autowired
    ProductService productService;

    @Autowired
    ExcelValidator excelValidator;

    @SuppressWarnings("unchecked")
    public List<ProductFileExcel> processExcelProduct(File file) throws Exception {
        List<ProductFileExcel> productInfos = new ArrayList<>();
        Map<String, Object> beans = new HashMap<>();

        InputStream inputXML = new BufferedInputStream(getClass().getResourceAsStream(PRODUCT_EXCEL_IMPORT_TEMPLATE));
        InputStream inputXLS = new BufferedInputStream(new FileInputStream(file));
        XLSReader mainReader = ReaderBuilder.buildFromXML(inputXML);

        beans.put("products", productInfos);
        logger.info("List Product Size " + productInfos.size());
        // đẩy dữ liệu từ file Excel trên server vào list
        mainReader.read(inputXLS, beans);
        productInfos = (List<ProductFileExcel>) beans.get("products");

        excelValidator.validate(productInfos);
        // gọi đến ProductService to handle Batch Insert
        productService.addListProductBatch(productInfos);
        return productInfos;
    }

}
