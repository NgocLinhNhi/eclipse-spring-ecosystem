package heavensky.spring.boot.shop.online.services_impl;

import heavensky.spring.boot.shop.online.repository.CategoryRepository;
import heavensky.spring.boot.shop.online.entity.Category;
import heavensky.spring.boot.shop.online.services.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    CategoryRepository categoryRepository;

    @Override
    public List<Category> loadAllCategory() {
        List<Category> loadAllCategory = categoryRepository.loadAllCategory();
        return loadAllCategory;
    }

}
