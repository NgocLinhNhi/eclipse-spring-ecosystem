package heavensky.spring.boot.shop.online.view;

public class ProductFileExcel {
	private Long category;

	private String productName;

	public ProductFileExcel(Long category, String productName) {
		super();
		this.category = category;
		this.productName = productName;
	}

	public ProductFileExcel() {
		super();
	}

	public Long getCategory() {
		return category;
	}

	public void setCategory(Long category) {
		this.category = category;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

}
