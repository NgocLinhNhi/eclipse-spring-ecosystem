package heavensky.spring.boot.shop.online.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "PRODUCT")
@NamedStoredProcedureQueries({
        @NamedStoredProcedureQuery(name = "getAllProducts", procedureName = "GET_ALL_PRODUCTS", resultClasses = Product.class)})
public class Product implements Serializable {
    private static final long serialVersionUID = -5538416125326898258L;

    @Id
    @Column(name = "SEQ_PRO")
    private Long seqPro;

    // name = tên cột trong bảng Product => Không phải bảng cha Category
    @ManyToOne
    @JoinColumn(name = "CATEGORY_ID", nullable = false)
    // cho insertable , updateable = false ở đây là khỏi insert update luôn -,-
    // @JsonIgnore => dung JsonIgnore  để không bị lỗi dataJson khi đưa data có bảng relation lên data
    @JsonBackReference
    private Category category;

    @Column(name = "PRODUCT_NAME")
    private String productName;

    @Column(name = "PRICE")
    private BigDecimal price;

    @Column(name = "IMAGE_URL")
    private String imageProduct;

    @Column(name = "NUMBER_SALES")
    private BigDecimal numberSales;

    @Column(name = "GUARANTEE")
    private BigDecimal guarantee;

    @Temporal(TemporalType.DATE)
    @Column(name = "CREATE_DATE")
    private Date createDate;

    @Temporal(TemporalType.DATE)
    @Column(name = "UPDATE_DATE")
    private Date updateDate;

    @Column(name = "SYS_STATUS")
    private Long sysStatus;

    @Column(name = "IS_USING")
    private String isUsing;

    //Mapped ở đây là tên biến trong Object( Java class ) con DetailBill
    //Set Lazy vì 1 product nhiều Bill => Slow
    @OneToMany(mappedBy = "product")
    private Set<DetailBill> detailBill;

    // Để join column trong table và manyToOne noó ko set được data cho object thì
    // chơi kiểu
    // tạo 1 object riêng ko map voi table trong DB
    // private transient Category categoryTest;

    private transient Long categoryId;

    private transient String categoryName;

    public Product() {
        super();
    }

    public Product(Long seqPro, Category category, String productName, BigDecimal price, String imageProduct,
                   BigDecimal numberSales, BigDecimal guarantee, Date createDate, Date updateDate, Long sysStatus,
                   Set<DetailBill> detailBill) {
        super();
        this.seqPro = seqPro;
        this.category = category;
        this.productName = productName;
        this.price = price;
        this.imageProduct = imageProduct;
        this.numberSales = numberSales;
        this.guarantee = guarantee;
        this.createDate = createDate;
        this.updateDate = updateDate;
        this.sysStatus = sysStatus;
        this.detailBill = detailBill;
    }

    public Long getSeqPro() {
        return seqPro;
    }

    public void setSeqPro(Long seqPro) {
        this.seqPro = seqPro;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getImageProduct() {
        return imageProduct;
    }

    public void setImageProduct(String imageProduct) {
        this.imageProduct = imageProduct;
    }

    public BigDecimal getNumberSales() {
        return numberSales;
    }

    public void setNumberSales(BigDecimal numberSales) {
        this.numberSales = numberSales;
    }

    public BigDecimal getGuarantee() {
        return guarantee;
    }

    public void setGuarantee(BigDecimal guarantee) {
        this.guarantee = guarantee;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Long getSysStatus() {
        return sysStatus;
    }

    public void setSysStatus(Long sysStatus) {
        this.sysStatus = sysStatus;
    }

    public Set<DetailBill> getDetailBill() {
        return detailBill;
    }

    public void setDetailBill(Set<DetailBill> detailBill) {
        this.detailBill = detailBill;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getIsUsing() {
        return isUsing;
    }

    public void setIsUsing(String isUsing) {
        this.isUsing = isUsing;
    }

}