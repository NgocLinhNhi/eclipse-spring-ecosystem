package heavensky.spring.boot.shop.online.services_impl;

import heavensky.spring.boot.shop.online.entity.User;
import heavensky.spring.boot.shop.online.repository.UserRepository;
import heavensky.spring.boot.shop.online.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;

    @Override
    public User loginUser(User us) {
        return userRepository.loginUser(us.getLoginName(), us.getPassword());
    }

    @Override
    public Boolean registerMember(User us) {
        long seqNo = selectMaxSeq() + 1;

        us.setSeqUser(seqNo);
        us.setRoleUser(1);
        us.setIsDelete(1);

        Date date = new Date();
        us.setCreateDate(date);

        User saveAndFlush = userRepository.saveAndFlush(us);

        return saveAndFlush != null;
    }

    @Override
    public long selectMaxSeq() {
        return userRepository.selectMaxUser();
    }

    @Override
    public Boolean checkExistLoginName(User us) {
        User result = userRepository.checkExistLoginName(us.getLoginName());
        return result == null;
    }

}