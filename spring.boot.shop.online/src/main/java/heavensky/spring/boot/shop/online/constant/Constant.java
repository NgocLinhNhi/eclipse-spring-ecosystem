package heavensky.spring.boot.shop.online.constant;

public class Constant {
    public static final String EXCEPTION = "Exception";
    public static final String EMPTY = "";
    public static final String SEND_EMAIL = "Send email has failed!!!";
    public static final String ABSOLUTE_PATH = "/static/report/";

    public static class RESPONSE {
        public static final int SUCCESS_STATUS = 200;
        public static final String SUCCESS_CODE = "SUCCESS";
        public static final String SUCCESS_MESSAGE = "Thực hiện thành công!";

        public static final int EXCEPTION_STATUS = 0;
        public static final String EXCEPTION_CODE = "EXCEPTION";
        public static final String EXCEPTION_MESSAGE = "Thực hiện không thành công!";

        public static final String EXIST_MESSAGE = "Dữ liệu đã được sử dụng, không được xóa";
        public static final String EXIST_USERNAME = "Tên đăng nhập đã tồn tại";
        public static final String SEARCH_SPECIAL = "%";
        public static final String SEARCH_SPECIAL_REGEX = "\\%";
        public static final String SEARCH_SPECIAL_REGEX_REPLACE = "\\\\%";

        //upload file
        public static final String FILE_ERROR = "Đã có lỗi trong quá trình xử lý file, vui lòng tải lại theo đúng mẫu quy định !!!";
        public static final String FILE_EXCEL_VALIDATE_ERROR = "Validate file has error, please download again template file !!!";
        public static final String FILE_EXCEL_IMPORT_ERROR = "import file has error, please download again template file !!!";
        public static final String FILE_LOADING = "Đang tải...";
        public static final String FILE_SUCCESS = "Thêm mới thành công ";
        public static final String FILE_TOTAL_DATA = " bản ghi ";

        // USER
        public static final String EXIST_USER_NAME = "Tên đăng nhập đã tồn tại!";
    }

    public static class FILENAME {
        public static final String PRODUCT_REPORT_EXCEL = "ExportProductTemplateExcel.xlsx";
        public static final String PRODUCT_EXCEL_IMPORT_TEMPLATE = "/static/report/ImportProductMappingTemplate.xml";
        public static final String CONTEN_REPORT = "6 THÁNG CUỐI";

    }
}
