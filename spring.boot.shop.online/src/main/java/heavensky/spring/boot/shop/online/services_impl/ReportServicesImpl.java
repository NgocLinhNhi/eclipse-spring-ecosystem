package heavensky.spring.boot.shop.online.services_impl;

import heavensky.spring.boot.shop.online.repository.ProductRepository;
import heavensky.spring.boot.shop.online.entity.Product;
import heavensky.spring.boot.shop.online.services.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import java.util.List;

@Service
public class ReportServicesImpl implements ReportService {
    @Autowired
    ProductRepository productRepository;

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<Product> reportListProduct() {
        return productRepository.loadAllProduct();
    }

    @Override
    public List<Product> reportListProductView() {
        return productRepository.loadAllProductView();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Product> exportExcelReportByStoreProcedure() {
        StoredProcedureQuery query = em.createNamedStoredProcedureQuery("getAllProducts");
        query.registerStoredProcedureParameter(1, Class.class, ParameterMode.REF_CURSOR);// outPut Parameter
        query.execute();

        return query.getResultList();

    }

}
