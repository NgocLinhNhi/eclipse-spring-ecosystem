package heavensky.spring.boot.shop.online.services;

import heavensky.spring.boot.shop.online.entity.User;

public interface UserService {

    User loginUser(User us);

    Boolean registerMember(User us);

    long selectMaxSeq();

    Boolean checkExistLoginName(User us);
}
