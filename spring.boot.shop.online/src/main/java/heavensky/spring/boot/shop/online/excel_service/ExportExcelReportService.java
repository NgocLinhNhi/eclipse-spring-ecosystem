package heavensky.spring.boot.shop.online.excel_service;

import heavensky.spring.boot.shop.online.constant.Constant;
import heavensky.spring.boot.shop.online.entity.Product;
import org.jxls.common.Context;
import org.jxls.util.JxlsHelper;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static heavensky.spring.boot.shop.online.constant.Constant.ABSOLUTE_PATH;
import static heavensky.spring.boot.shop.online.constant.Constant.FILENAME.CONTEN_REPORT;

@Service
public class ExportExcelReportService {

    public void exportReportProduct(HttpServletResponse response,
                                    Date startDate,
                                    Date endDate,
                                    List<Product> rptList) throws IOException {
        InputStream is = createInputStream();
        Context context = createContext(startDate, endDate, rptList);
        // cái này để mở chế độ suggest download cho file = response
        response.setContentType("application/octet-stream");
        response.setHeader("Content-Disposition", "inline; filename=\"" + Constant.FILENAME.PRODUCT_REPORT_EXCEL + "\"");
        JxlsHelper.getInstance().processTemplate(is, response.getOutputStream(), context);
    }

    private InputStream createInputStream() {
        return getClass().getResourceAsStream(ABSOLUTE_PATH + Constant.FILENAME.PRODUCT_REPORT_EXCEL);
    }

    private Context createContext(Date startDate, Date endDate, List<Product> rptList) {
        Calendar c = Calendar.getInstance();
        Context context = new Context();
        c.setTime(startDate);
        c.setTime(endDate);
        context.putVar("rpTitle", CONTEN_REPORT);
        context.putVar("rpTitleYear", c.get(Calendar.YEAR));

        // rpMembers biến edit trong comment của file excel - cell để định nghĩa
        context.putVar("rpMembers", rptList);
        return context;
    }
}