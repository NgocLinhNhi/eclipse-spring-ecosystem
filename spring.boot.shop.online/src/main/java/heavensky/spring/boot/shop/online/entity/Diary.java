package heavensky.spring.boot.shop.online.entity;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "DIARYADMIN")
public class Diary implements Serializable {

    private static final long serialVersionUID = -731200869916708312L;

    @Id
    @Column(name = "SEQ_NO")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int seqNo;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ADMIN_ID", nullable = false)
    @Fetch(FetchMode.SELECT)
    private Admin admin;

    @Temporal(TemporalType.DATE)
    @Column(name = "CREATE_DATE")
    private Date createDate;

    @Column(name = "CONTENT_DIARY")
    private String contentDiary;

    public int getSeqNo() {
        return seqNo;
    }

    public void setSeqNo(int seqNo) {
        this.seqNo = seqNo;
    }

    public Admin getAdmin() {
        return admin;
    }

    public void setAdmin(Admin admin) {
        this.admin = admin;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getContentDiary() {
        return contentDiary;
    }

    public void setContentDiary(String contentDiary) {
        this.contentDiary = contentDiary;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    public Diary(int seqNo, Admin admin, Date createDate, String contentDiary) {
        super();
        this.seqNo = seqNo;
        this.admin = admin;
        this.createDate = createDate;
        this.contentDiary = contentDiary;
    }

    public Diary() {
        super();
    }

}
