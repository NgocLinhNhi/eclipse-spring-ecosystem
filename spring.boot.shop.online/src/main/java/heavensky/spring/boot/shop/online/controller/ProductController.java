package heavensky.spring.boot.shop.online.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import heavensky.spring.boot.shop.online.constant.Constant;
import heavensky.spring.boot.shop.online.entity.Product;
import heavensky.spring.boot.shop.online.exception.GatewayNotFoundException;
import heavensky.spring.boot.shop.online.message.Response;
import heavensky.spring.boot.shop.online.services.ProductService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.ModelMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@RestController
@RequestMapping(value = "/product")
public class ProductController {

    @Autowired
    ProductService productService;

    private static Logger logger = Logger.getLogger(ProductController.class);

    @GetMapping(value = "/loadAllProduct")
    public ResponseEntity<List<Product>> loadAllProduct() {
        // Load all thì có category nhưng ép kiểu JSon thì nó không cho .
        List<Product> loadAllProduct = productService.loadAllProduct();
        return new ResponseEntity<>(loadAllProduct, HttpStatus.OK);
    }

    @GetMapping(value = "/loadAllProductJson")
    public String loadAllProductJson() {
        String result = null;
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            List<Product> loadAllProduct = productService.loadAllProduct();
            result = objectMapper.writeValueAsString(loadAllProduct);
        } catch (Exception e) {
            logger.error(Constant.EXCEPTION, e);
        }
        return result;
    }

    // sử dụng trong trường hợp update hay view = modal tại trang hiện tại
    @GetMapping(value = "/findProductBySeq/{seqPro}")
    public ResponseEntity<Product> getProductBySeqPro(@PathVariable long seqPro) {
        Product proFind = null;
        try {
            proFind = productService.findProductById(seqPro);
        } catch (GatewayNotFoundException e) {
            return new ResponseEntity<>(proFind, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(proFind, HttpStatus.OK);
    }

    // trường hợp move sang 1 page jsp khác để update hay view
    @GetMapping(value = "/updateProductBySeq/{seqPro}")
    public ModelAndView updateProductBySeqPro(@PathVariable String seqPro, ModelMap model) {
        // do truyền = href no hiểu là String nên để ở tham số là String
        Long seq = Long.parseLong(seqPro);
        Product proFind = productService.findProductById(seq);
        if (proFind != null) {
            model.addAttribute("product", proFind);
        }
        return new ModelAndView("pages/updateProduct");
    }

    @PostMapping(value = "/addNewProduct", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Response> addNewProduct(@RequestBody @Validated Product pro) {
        try {
            productService.addNewProduct(pro);
        } catch (Exception e) {
            logger.error(Constant.EXCEPTION, e);
            return new ResponseEntity<>(new Response(
                    Constant.RESPONSE.EXCEPTION_STATUS,
                    Constant.RESPONSE.EXCEPTION_CODE,
                    Constant.RESPONSE.EXCEPTION_MESSAGE),
                    HttpStatus.EXPECTATION_FAILED);
        }
        return new ResponseEntity<>(new Response(
                Constant.RESPONSE.SUCCESS_STATUS,
                Constant.RESPONSE.SUCCESS_CODE,
                Constant.RESPONSE.SUCCESS_MESSAGE),
                HttpStatus.OK);
    }

    @PostMapping(value = "/deleteProduct/{seqPro}")
    public ResponseEntity<Response> deleteProduct(@PathVariable long seqPro) {
        try {
            productService.deleteProduct(seqPro);
        } catch (Exception e) {
            logger.error(Constant.EXCEPTION, e);
            return new ResponseEntity<>(new Response(
                    Constant.RESPONSE.EXCEPTION_STATUS,
                    Constant.RESPONSE.EXCEPTION_CODE,
                    Constant.RESPONSE.EXIST_MESSAGE),
                    HttpStatus.EXPECTATION_FAILED);
        }
        return new ResponseEntity<>(new Response(
                Constant.RESPONSE.SUCCESS_STATUS,
                Constant.RESPONSE.SUCCESS_CODE,
                Constant.RESPONSE.SUCCESS_MESSAGE),
                HttpStatus.CREATED);
    }

    @PostMapping(value = "/updateProduct")
    public ResponseEntity<Response> updateProduct(@RequestBody Product pro) {
        try {
            productService.updateProduct(pro);
        } catch (Exception e) {
            logger.error(Constant.EXCEPTION, e);
            return new ResponseEntity<>(new Response(
                    Constant.RESPONSE.EXCEPTION_STATUS,
                    Constant.RESPONSE.EXCEPTION_CODE,
                    Constant.RESPONSE.EXIST_MESSAGE),
                    HttpStatus.EXPECTATION_FAILED);
        }
        return new ResponseEntity<>(new Response(
                Constant.RESPONSE.SUCCESS_STATUS,
                Constant.RESPONSE.SUCCESS_CODE,
                Constant.RESPONSE.SUCCESS_MESSAGE),
                HttpStatus.OK);
    }
}
