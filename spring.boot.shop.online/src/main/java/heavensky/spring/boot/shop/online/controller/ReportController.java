package heavensky.spring.boot.shop.online.controller;

import heavensky.spring.boot.shop.online.constant.Constant;
import heavensky.spring.boot.shop.online.entity.Product;
import heavensky.spring.boot.shop.online.excel_service.ExportExcelReportService;
import heavensky.spring.boot.shop.online.services.ReportService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping(value = "/report")
public class ReportController {

    private static Logger logger = Logger.getLogger(ReportController.class);

    @Autowired
    ReportService reportService;

    @Autowired
    ExportExcelReportService exportReportService;

    @RequestMapping(value = "/productReport")
    public ModelAndView goToReport() {
        return new ModelAndView("pages/report");
    }


    @RequestMapping(value = "/importFileExcelProduct")
    public ModelAndView goAddProductFile() {
        return new ModelAndView("pages/importProductByFileExcel");
    }

    @PostMapping(value = "/exportReportProduct")
    public void exportReportProduct(HttpServletResponse response) {
        //Tạo Report normal
        List<Product> reportListProduct = reportService.reportListProduct();

        /*tạo report thông qua gọi Stored procedure */
        //List<Product> reportListProduct = reportServicesImpl.exportExcelReportByStoreProcedure();

        /*tạo report thông qua view Trong database*/
        //List<Product> reportListProduct = reportServicesImpl.reportListProductView();
        Date date = new Date();
        try {
            exportReportService.exportReportProduct(response, date, date, reportListProduct);
        } catch (IOException e) {
            logger.error(Constant.EXCEPTION, e);
        }
    }
}
