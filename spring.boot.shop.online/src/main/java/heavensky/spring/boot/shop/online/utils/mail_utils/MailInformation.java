package heavensky.spring.boot.shop.online.utils.mail_utils;

public class MailInformation {
	private String host;
	private String port;
	private String account;
	private String password;
	private String timeOut;

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getTimeOut() {
		return timeOut;
	}

	public void setTimeOut(String timeOut) {
		this.timeOut = timeOut;
	}

	public MailInformation(String host, String port, String account, String password, String timeOut) {
		super();
		this.host = host;
		this.port = port;
		this.account = account;
		this.password = password;
		this.timeOut = timeOut;
	}

	public MailInformation() {
		super();
	}

	@Override
	public String toString() {
		return "MailInformation [host=" + host + ", port=" + port + ", account=" + account + ", password=" + password
				+ ", timeOut=" + timeOut + "]";
	}

}
