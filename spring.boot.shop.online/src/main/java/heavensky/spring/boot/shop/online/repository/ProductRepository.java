package heavensky.spring.boot.shop.online.repository;

import heavensky.spring.boot.shop.online.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

import static heavensky.spring.boot.shop.online.sql.ProductSqlQuery.*;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

    @Query(LOAD_ALL_PRODUCT)
    List<Product> loadAllProduct();

    // get data từ View được tạo trong Database
    @Query(LOAD_PRODUCT_BY_VIEW)
    List<Product> loadAllProductView();

    @Query(FIND_PRODUCT_BY_ID)
    Product findProductById(@Param("seqPro") Long seqPro);

    @Query(value = MAX_SEQ_PRODUCT, nativeQuery = true)
    long selectMaxSeqProduct();

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query(UPDATE_PRODUCT)
    int updateStatusProduct(Long id);
}
