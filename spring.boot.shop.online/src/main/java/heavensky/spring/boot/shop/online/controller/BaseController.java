package heavensky.spring.boot.shop.online.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;

@Controller
public class BaseController {

    @RequestMapping("/")
    public ModelAndView home(Map<String, Object> model) {
        ModelAndView mv = new ModelAndView("index");
        model.put("message", "WELCOME TO SHOP ONLINE!!");
        return mv;
    }

    @GetMapping(value = "/goLogin")
    public ModelAndView showLoginPage() {
        return new ModelAndView("user/login");
    }

    @GetMapping(value = "/goRegister")
    public ModelAndView showRegisterPage() {
        return new ModelAndView("user/registerStepByStepType2");
        //return new ModelAndView("user/registerStepByStepType1");
    }

    @GetMapping(value = "/logout")
    public ModelAndView logOut() {
        return new ModelAndView("user/logout");
    }

    @GetMapping(value = "/product/addNewProduct")
    public ModelAndView addProduct() {
        return new ModelAndView("pages/addProduct");
    }

}
