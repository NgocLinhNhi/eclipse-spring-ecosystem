<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>

<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>
<head>
    <title>Thêm mới sản phẩm</title>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>

    <script src="/jquerry/jquery.min.js"></script>
    <script src="/jquerry/jquery.validate.min.js"></script>
    <!-- for validate form -->
    <script type="text/javascript" src="/jquerry/jquery.dataTables.min.js"></script>

    <!-- de bootstrap import dưới jquery ko bi conflict thu vien goi boostrap của jquerry ko gọi đc modal popup -->
    <link rel="stylesheet" type="text/css" href="/bootstrap/js/bootstrap.min.css">
    <script src="/bootstrap/js/bootstrap.min.js"></script>

    <script src="/js/commonAjax.js"></script>
    <script src="/css/ajaxLoading.css"></script>
    <link rel="stylesheet" type="text/css" href="/css/ErrorMessage.css">


</head>
<body>
<div class="container" style="margin-top: 50px;">
    <div class="col-sm-10"></div>
    <div class="col-md-3"></div>
    <div class="col-md-6">

        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Thêm sản phẩm</h3>
            </div>

            <!--  -->
            <form class="form-horizontal" id="add-Product-form" action=""  method="POST">
                <div class="box-body">
                    <div class="form-group">
                        <label for="productName" class="col-sm-3 control-label">Product Name</label>
                        <div class="col-sm-9">
                            <input type="text"
                                   class="form-control"
                                   id="productName"
                                   name="productName"
                                   placeholder="Tên sản phẩm">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Category</label>
                        <div class="col-md-9">
                            <select id="categoryId"
                                    name="categoryId"
                                    class="form-control select2">
                                <option value="" selected="selected">-- Chọn --</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="price" class="col-sm-3 control-label">Price</label>
                        <div class="col-sm-9">
                            <input type="number"
                                   class="form-control"
                                   id="price"
                                   name="price"
                                   placeholder="Giá cả">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="numberSales" class="col-lg-3 control-label">number
                            sales</label>

                        <div class="col-lg-9">
                            <input type="number"
                                   class="form-control"
                                   id="numberSales"
                                   name="numberSales"
                                   placeholder="Số lượng">

                        </div>
                    </div>

                    <div class="form-group">
                        <label for="guarantee" class="col-sm-3 control-label">Guarantee</label>
                        <div class="col-sm-9">
                            <input type="number"
                                   class="form-control"
                                   id="guarantee"
                                   name="guarantee"
                                   placeholder="Bảo hành">
                        </div>
                    </div>

                </div>

                <div class="box-footer">
                    <button type="submit"
                            name="save"
                            id="btn-save"
                            class="btn btn-primary pull-right">Add
                    </button>
                </div>
            </form>
        </div>
    </div>
    <div class="col-md-3"></div>

    <!-- Modal thông báo message add-->
    <div class="modal fade" id="notifiModal" role="dialog">
        <div class="modal-dialog">

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close dongmodal" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Thông báo</h4>
                </div>
                <div class="modal-body">
                    <p id="massagemodal"></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default dongmodal"
                            data-dismiss="modal">Đóng
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- endmodal -->
</div>

<div class="fullscreen">
    <div id="ajaxLoading"></div>
</div>

<script type="text/javascript">
    CommonAjax.get(null, "/cate/loadAllCategory", getInfoSuccess, null);

    function getInfoSuccess(data) {
        var categoryInfo = data;
        for (var i = 0; i < categoryInfo.length; i++) {
            var cate = categoryInfo[i];
            $("#categoryId").append('<option value="' + cate.categoryId + '">' + cate.categoryName + '</option>')
        }
    }

    //validate form
    var formValidator = $("#add-Product-form").validate({
        rules: {
            productName: {
                required: true,
                maxlength: 20
            },
            price: {
                maxlength: 20
            },
            numberSales: {
                maxlength: 20
            },
            guarantee: {
                maxlength: 3
            }
        },
        messages: {
            productName: {
                required: "Vui lòng nhập tên sản phẩm",
                maxlength: "Độ dài họ tên không vượt quá 20 kí tự"
            },
            price: {
                maxlength: "Độ dài password không vượt quá 20 kí tự"
            },
            numberSales: {
                maxlength: "Độ dài password không vượt quá 20 kí tự"
            },
            guarantee: {
                maxlength: "Độ dài password không vượt quá 3 kí tự"
            }
        }
    });

    $("#btn-save").click(function () {
        formValidator.settings.submitHandler = addNewProduct;
    });

    function addNewProduct() {
        var productNameInput = $("#productName").val().trim();
        var priceInput = $("#price").val().trim();
        var numberSaleInput = $('#numberSales').val().trim();
        var guaranteeInput = $("#guarantee").val().trim();
        var categoryIdInput = $('#categoryId').val().trim();

        var productName = productNameInput !== "" ? productNameInput : null;
        var price = priceInput !== "" ? priceInput : null;
        var numberSales = numberSaleInput !== "" ? numberSaleInput : null;
        var guarantee = guaranteeInput !== "" ? guaranteeInput : null;
        var categoryId = categoryIdInput !== "" ? categoryIdInput : null;

        var memberInfo = {
            "productName": productName, // "trong string la chuoi map voi field trong object class entity"
            "price": price,
            "numberSales": numberSales,
            "guarantee": guarantee,
            "category": {
                "categoryId": categoryId
            }
        };
        CommonAjax.post(memberInfo, "/product/addNewProduct", addSuccess, addError);
    }

    function addSuccess(data) {
        var modal_header = $("#notifiModal .modal-header");
        var notify = $("#notifiModal");

        modal_header.removeClass("btn-danger");
        modal_header.addClass("btn-success");
        notify.addClass("fade");
        $('#massagemodal').html(data.message);//add message vao message của popup
        notify.modal("toggle"); //hiển thị popup
        notify.on('hidden.bs.modal', function () {
            location.reload();
        });
    }

    function addError(data) {
        var modal_header = $("#notifiModal .modal-header");
        var notify = $("#notifiModal");

        modal_header.removeClass("btn-success");
        modal_header.addClass("btn-danger");
        notify.addClass("fade");
        $('#massagemodal').html(data.message);//chuyển đổi message sang dạng Json của thư viện jquery
        notify.modal("toggle");
        notify.on('hidden.bs.modal', function () {
            $(".modal-backdrop").remove();
            $("#notifiModal").removeAttr("style");
        });
    }
</script>
</body>
</html>