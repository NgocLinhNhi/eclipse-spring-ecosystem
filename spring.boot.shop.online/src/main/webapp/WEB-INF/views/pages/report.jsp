<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<html>
<head>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <title>Xuất báo cáo sản phẩm</title>

    <script src="/jquerry/jquery.min.js"></script>
    <script src="/jquerry/jquery.validate.min.js"></script>
    <!-- for validate form -->
    <script type="text/javascript" src="/jquerry/jquery.dataTables.min.js"></script>

    <!-- de bootstrap import dưới jquery ko bi conflict thu vien goi boostrap của jquerry ko gọi đc modal popup -->
    <link rel="stylesheet" type="text/css"
          href="/bootstrap/js/bootstrap.min.css">
    <script src="/bootstrap/js/bootstrap.min.js"></script>
    <!-- validate cho datatable data hien thi -->
    <script src="/DataTables/js/dataTables.bootstrap.min.js"></script>

    <script src="/js/commonAjax.js"></script>
    <link rel="stylesheet" type="text/css" href="/css/ErrorMessage.css">
</head>


<body>
<div class="content-wrapper" style="margin-top: 30px;">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-default">

                <table id="todotable" class="table table-striped">
                    <thead>
                    <tr>
                        <th>Export Excel Report for Product</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    <div class="box-body">
        <div class="footform">
            <form action="/report/exportReportProduct" method="POST" id="export-form"
                  class="form-horizontal col-md-12" role="form">

                <div class="col-md-12 form-group">
                    <div class="col-md-3">
                        <button id="filter" type="submit" class="btn btn-primary">Export</button>
                    </div>
                </div>
            </form>

        </div>
    </div>
</div>

</body>

</html>