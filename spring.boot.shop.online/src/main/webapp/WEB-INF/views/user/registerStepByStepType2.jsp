<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<html>
<head>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <title>Register Form</title>

    <script src="/jquerry/jquery.min.js"></script>
    <script src="/jquerry/jquery.validate.min.js"></script>
    <script type="text/javascript" src="/jquerry/jquery.dataTables.min.js"></script>

    <!-- de bootstrap import dưới jquery ko bi conflict thu vien goi boostrap của jquerry ko gọi đc modal popup -->
    <link rel="stylesheet" type="text/css" href="/bootstrap/js/bootstrap.min.css">
    <script src="/bootstrap/js/bootstrap.min.js"></script>

    <script src="/js/commonAjax.js"></script>
    <link rel="stylesheet" type="text/css" href="/css/ErrorMessage.css">

    <style type="text/css">
        #personal_information {
            display: none;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="col-lg-5">
        <form class="form-horizontal" action="javascript:void(0);" id="myform" method="POST">
            <!--  action="javascript:void(0);" Không có cái này bỏ method = post vào chết ngay ->
                với có nó mới hiển thị đc popup với multipleSteps  => mỗi fieldset là 1 step  -->

            <!-- Step 1  -->
            <fieldset id="account_information" class="">
                <legend>Account information</legend>
                <div class="form-group">
                    <label for="loginName" class="col-sm-3 control-label">Login</label>
                    <div class="col-sm-9">
                        <input type="text"
                               class="form-control"
                               id="loginName"
                               name="loginName"
                               placeholder="Tên đăng nhập">
                    </div>
                </div>

                <div class="form-group">
                    <label for="password" class="col-sm-3 control-label">Password</label>
                    <div class="col-sm-9">
                        <input type="password"
                               class="form-control"
                               id="password"
                               name="password"
                               placeholder="Mật khẩu">
                    </div>
                </div>

                <div class="form-group">
                    <label for="conf_password" class="col-lg-3 control-label">Confirm password</label>
                    <div class="col-lg-9">
                        <input type="password"
                               class="form-control"
                               id="conf_password"
                               name="conf_password"
                               placeholder="Password">
                    </div>
                </div>
                <p><a class="btn btn-primary next" id="btnNext">next</a></p>
            </fieldset>

            <!-- Step 2  -->
            <fieldset id="personal_information" class="">
                <legend>Personal information</legend>
                <div class="form-group">
                    <label for="email" class="col-sm-3 control-label">Email</label>
                    <div class="col-sm-9">
                        <input type="email"
                               class="form-control"
                               id="email"
                               name="email"
                               placeholder="Địa chỉ EMail">
                    </div>
                </div>

                <div class="form-group">
                    <label for="phone" class="col-sm-3 control-label">Phone</label>
                    <div class="col-sm-9">
                        <input type="number"
                               class="form-control"
                               id="phone"
                               name="phone"
                               placeholder="Số điện thoại">
                    </div>
                </div>

                <div class="form-group">
                    <label for="address" class="col-sm-3 control-label">Address</label>
                    <div class="col-sm-9">
                        <input type="text"
                               class="form-control"
                               id="address"
                               name="address"
                               placeholder="Địa chỉ">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Gender </label>
                    <div class="col-md-8" style="padding-left: 0;">
                        <div class="col-md-6" style="margin-left: 0;">
                            Nam <input type="radio" value="1" name="gender" checked="checked">
                        </div>
                        <div class="col-md-6">
                            Nữ <input type="radio" value="0" name="gender">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="userName" class="col-sm-3 control-label">User Name</label>
                    <div class="col-sm-9">
                        <input type="text"
                               class="form-control"
                               id="userName"
                               name="userName"
                               placeholder="Tên khách hàng">
                    </div>
                </div>

                <div class="form-group">
                    <label for="identityCard" class="col-sm-3 control-label">Identity Card</label>
                    <div class="col-sm-9">
                        <input type="text"
                               class="form-control"
                               id="identityCard"
                               name="identityCard"
                               maxlength="20"
                               placeholder="Số chứng minh nhân dân">
                    </div>
                </div>

                <div class="box-footer">
                    <a class="btn btn-primary" id="previous">Previous</a>
                    <button type="submit" name="save" id="btn-save" class="btn btn-primary pull-right">Đăng ký</button>
                </div>
            </fieldset>

        </form>
    </div>
</div>

<div class="col-md-3"></div>

<!-- Modal thông báo message add-->
<div class="modal fade" id="notifiModal" role="dialog">
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close dongmodal" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Thông báo</h4>
            </div>
            <div class="modal-body">
                <p id="massagemodal"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default dongmodal"
                        data-dismiss="modal">Đóng
                </button>
            </div>
        </div>
    </div>
</div>
<!-- endmodal -->

<script type="text/javascript">
    $(document).ready(function () {
        // Custom method to validate username
        $.validator.addMethod("usernameRegex", function (value, element) {
            return this.optional(element) || /^[a-zA-Z0-9]*$/i.test(value);
        }, "Username must contain only letters, numbers");

        //tạo biến ngoài để validate khi click đăng ký ko bị insert luôn
        var form;
        $("#btnNext").click(function () {
            form = $("#myform");
            form.validate({
                errorElement: 'span',
                errorClass: 'help-block',
                //set up chế độ error cho các field được validate
                highlight: function (element) {
                    $(element).closest('.form-group').addClass("has-error");
                },
                unhighlight: function (element) {
                    $(element).closest('.form-group').removeClass("has-error");
                },
                rules: {
                    loginName: {
                        required: true,
                        maxlength: 255,
                        validateLoginName: true
                    },
                    password: {
                        required: true
                    },
                    conf_password: {
                        required: true,
                        equalTo: '#password'
                    },
                    email: {
                        required: true,
                        maxlength: 255
                    },
                    phone: {
                        maxlength: 11
                    },
                    address: {
                        maxlength: 255
                    },
                    gender: {
                        required: true
                    },
                    userName: {
                        maxlength: 30
                    },
                    identityCard: {
                        required: true,
                        maxlength: 20,
                        number: true
                    }

                },
                messages: {
                    loginName: {
                        required: "Vui lòng nhập họ tên",
                        maxlength: "Độ dài họ tên không vượt quá 255 kí tự",
                        validateLoginName: "Không nhập các ký tự đặc biệt"
                    },
                    password: {
                        required: "Vui lòng nhập password"
                    },
                    conf_password: {
                        required: "Vui lòng nhập password",
                        equalTo: "Password don't match"
                    },
                    email: {
                        required: "Vui lòng nhập địa chỉ email",
                        maxlength: "Độ dài email không vượt quá 255 kí tự"
                    },
                    phone: {
                        maxlength: "Độ dài phone không vượt quá 11 số"
                    },
                    address: {
                        maxlength: "Độ dài address không vượt quá 30 ký tư"
                    },
                    gioitinh: "Vui lòng chọn giới tính",
                    userName: {
                        maxlength: "Độ dài tên thật không vượt quá 255 ký tư"
                    },
                    identityCard: {
                        number: "Số CMND chỉ được bao gồm số",
                        required: "Vui lòng nhập vào số CMND",
                        maxlength: "Độ dài không được vượt quá 20 kí tự"
                    }
                }
            });

            //setup chế độ ân hiện div các steps - đơn giản hơn của fieldset
            if (form.valid() === true) {
                var account_information = $('#account_information');
                var personal_information = $('#personal_information');

                if (account_information.is(":visible")) {
                    current_fs = account_information;
                    next_fs = personal_information;
                } else if (personal_information.is(":visible")) {
                    current_fs = personal_information;
                }

                next_fs.show();
                current_fs.hide();
            }
        });

        $('#previous').click(function () {
            var personal_information = $('#personal_information');
            if (personal_information.is(":visible")) {
                current_fs = personal_information;
                next_fs = $('#account_information');
            }
            next_fs.show();
            current_fs.hide();
        });


        $("#btn-save").click(function () {
            if (form.valid() === true) {
                registerMember();
            }
        });

        $.validator.addMethod('validateLoginName', function () {
            var regex = /^[a-zA-Z0-9/_]+$/;
            var value = $("#loginName").val();
            return regex.test(value);
        }, 'Không nhập các ký tự đặc biệt');

    });

    function registerMember() {
        var loginNameInput = $("#loginName").val().trim();
        var passInput = $("#password").val().trim();
        var emailInput = $('#email').val().trim();
        var phoneInput = $("#phone").val().trim();
        var addressInput = $("#address").val().trim();
        var genderInput = $('input[name="gender"]:checked').val();
        var userNameInput = $("#userName").val().trim();
        var identityCardInput = $('#identityCard').val().trim();

        var loginName = loginNameInput !== "" ? loginNameInput : null;
        var password = passInput !== "" ? passInput : null;
        var email = emailInput !== "" ? emailInput : null;
        var phone = phoneInput !== "" ? phoneInput : null;
        var address = addressInput !== "" ? addressInput : null;
        var gender = genderInput !== "" ? genderInput : null;
        var userName = userNameInput !== "" ? userNameInput : null;
        var identityCard = identityCardInput !== "" ? identityCardInput : null;

        var memberInfo = {
            "loginName": loginName,
            "password": password,
            "email": email,
            "phone": phone,
            "address": address,
            "gender": gender,
            "userName": userName,
            "identityCard": identityCard
        };
        CommonAjax.post(memberInfo, "/user/register", addSuccess, addError);
    }

    function addSuccess(data) {
        var modal_header = $("#notifiModal .modal-header");
        var notify_modal = $("#notifiModal");

        modal_header.removeClass("btn-danger");
        modal_header.addClass("btn-success");
        notify_modal.addClass("fade");
        $('#massagemodal').html(data.message);//add message vao message của popup
        notify_modal.modal("toggle"); //hiển thị popup
        notify_modal.on('hidden.bs.modal', function () {
            location.reload();
        });
    }

    function addError(data) {
        var modal_header = $("#notifiModal .modal-header");
        var notify_modal = $("#notifiModal");

        modal_header.removeClass("btn-success");
        modal_header.addClass("btn-danger");
        notify_modal.addClass("fade");
        $('#massagemodal').html(data.responseJSON.message);//chuyển đổi message sang dạng Json của thư viện jquery
        notify_modal.modal("toggle");
        notify_modal.on('hidden.bs.modal', function () {
            $(".modal-backdrop").remove();
            $("#notifiModal").removeAttr("style");
        });
    }
</script>
</body>
</html>