<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>
<head>
    <title>Register Page</title>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>

    <script src="/jquerry/jquery.min.js"></script>
    <script src="/jquerry/jquery.validate.min.js"></script> <!-- for validate form -->
    <script type="text/javascript" src="/jquerry/jquery.dataTables.min.js"></script>
    <!-- de bootstrap import dưới jquery ko bi conflict thu vien goi boostrap của jquerry ko gọi đc modal popup -->
    <link rel="stylesheet" type="text/css" href="/bootstrap/js/bootstrap.min.css">
    <script src="/bootstrap/js/bootstrap.min.js"></script>
    <script src="/js/commonAjax.js"></script>
    <script src="/css/ajaxLoading.css"></script>
    <link rel="stylesheet" type="text/css" href="/css/ErrorMessage.css">
    <link rel="stylesheet" type="text/css" href="/css/loginsteps.css">
</head>

<body>
<div style="height:40px;"></div>
<div class="assessment-container container">
    <div class="row">
        <div class="col-md-6 form-box">
            <form class="registration-form" action="javascript:void(0);" method="POST">

                <!--  action="javascript:void(0);" Không có cái này bỏ method = post vào chết ngay
                -> với có nó mới hiển thị đc popup với multisteps -->
                <fieldset>
                    <div class="form-top">
                        <div class="form-top-left">
                            <h3>
                                <span>
                                    <i class="fa fa-calendar-check-o" aria-hidden="true"> </i>
                                </span> Register member - Step 1
                            </h3>
                        </div>
                    </div>

                    <div class="form-bottom">
                        <div class="row">
                            <label for="loginName" class="col-sm-3 control-label">Tên đăng nhập</label>
                            <div class="form-group col-md-6 col-sm-6">
                                <input type="text"
                                       class="form-control"
                                       id="loginName"
                                       name="loginName"
                                       placeholder="Tên đăng nhập">
                            </div>
                        </div>

                        <div class="row">
                            <label for="password" class="col-sm-3 control-label">Mật khẩu</label>
                            <div class="form-group col-md-6 col-sm-6">
                                <input type="password"
                                       class="form-control"
                                       id="password"
                                       name="password"
                                       placeholder="Mật khẩu">
                            </div>
                        </div>

                        <button type="button" class="btn btn-next">Next</button>
                    </div>
                </fieldset>

                <fieldset>
                    <div class="form-top">
                        <div class="form-top-left">
                            <h3>
                                <span>
                                    <i class="fa fa-calendar-check-o" aria-hidden="true"></i>
                                </span> Steps 2
                            </h3>
                        </div>
                    </div>

                    <div class="form-bottom">
                        <div class="row">
                            <label for="email" class="col-sm-3 control-label">Email</label>
                            <div class="col-sm-9">
                                <input type="text"
                                       class="form-control"
                                       id="email"
                                       name="email"
                                       placeholder="Địa chỉ EMail">
                            </div>
                        </div>

                        <div class="row">
                            <label for="phone" class="col-sm-3 control-label">Phone</label>
                            <div class="col-sm-9">
                                <input type="text"
                                       class="form-control"
                                       id="phone"
                                       name="phone"
                                       placeholder="Số điện thoại">
                            </div>
                        </div>


                        <div class="row">
                            <label for="address" class="col-sm-3 control-label">Address</label>
                            <div class="col-sm-9">
                                <input type="text"
                                       class="form-control"
                                       id="address"
                                       name="address"
                                       placeholder="Địa chỉ">
                            </div>
                        </div>

                        <div class="row">
                            <label class="col-sm-3 control-label">Gender </label>
                            <div class="col-md-8" style="padding-left: 0;">
                                <div class="col-md-6" style="margin-left: 0;">
                                    Nam
                                    <input type="radio"
                                           value="1"
                                           name="gender"
                                           checked="checked">
                                </div>
                                <div class="col-md-6">
                                    Nữ
                                    <input type="radio"
                                           value="0"
                                           name="gender">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <label for="userName" class="col-sm-3 control-label">User Name</label>
                            <div class="col-sm-9">
                                <input type="text"
                                       class="form-control"
                                       id="userName"
                                       name="userName"
                                       placeholder="Tên khách hàng">
                            </div>
                        </div>

                        <div class="row">
                            <label for="identityCard" class="col-sm-3 control-label">Identity Card</label>
                            <div class="col-sm-9">
                                <input type="text"
                                       class="form-control"
                                       id="identityCard"
                                       name="identityCard"
                                       maxlength="20"
                                       placeholder="Số chứng minh nhân dân">
                            </div>
                        </div>

                        <button type="button" class="btn btn-previous">Previous</button>
                        <button type="submit"
                                name="save"
                                id="btn-save"
                                class="btn btn-primary pull-right">Đăng ký
                        </button>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
</div>

<!-- Modal thông báo message add-->
<div class="modal fade" id="notifiModal" role="dialog">
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close dongmodal" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Thông báo</h4>
            </div>
            <div class="modal-body">
                <p id="massagemodal"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default dongmodal"
                        data-dismiss="modal">Đóng
                </button>
            </div>
        </div>
    </div>
</div>
<!-- endmodal -->

<div class="fullscreen">
    <div id="ajaxLoading"></div>
</div>

<script type="text/javascript">

    //xu ly nhieu steps register
    $(document).ready(function () {
        $('.registration-form fieldset:first-child').fadeIn('slow');

        $('.registration-form input[type="text"]').on('focus', function () {
            $(this).removeClass('input-error');
        });

        // next step
        $('.registration-form .btn-next').on('click', function () {
            var parent_fieldset = $(this).parents('fieldset');
            var next_step = true;

            parent_fieldset.find('input[type="text"],input[type="email"]').each(function () {
                if ($(this).val() === "") {
                    $(this).addClass('input-error');
                    next_step = false;
                } else {
                    $(this).removeClass('input-error');
                }
            });

            if (next_step) {
                parent_fieldset.fadeOut(400, function () {
                    $(this).next().fadeIn();
                });
            }

        });

        // previous step
        $('.registration-form .btn-previous').on('click', function () {
            $(this).parents('fieldset').fadeOut(400, function () {
                $(this).prev().fadeIn();
            });
        });

        // submit
        /*         $('.registration-form').on('submit', function (e) {
                    $(this).find('input[type="text"],input[type="email"]').each(function () {
                        if ($(this).val() == "") {
                            e.preventDefault();
                            $(this).addClass('input-error');
                        } else {
                            $(this).removeClass('input-error');
                        }
                    });

                }); */
    });
    //xu ly nhieu steps register

    $("#btn-save").click(function () {
        registerMember();
    });

    function registerMember() {
        var loginNameInput = $("#loginName").val().trim();
        var passInput = $("#password").val().trim();
        var emailInput = $('#email').val().trim();
        var phoneInput = $("#phone").val().trim();
        var addressInput = $("#address").val().trim();
        var genderInput = $('input[name="gender"]:checked').val();
        var userNameInput = $("#userName").val().trim();
        var identityCardInput = $('#identityCard').val().trim();

        var loginName = loginNameInput !== "" ? loginNameInput : null;
        var password = passInput !== "" ? passInput : null;
        var email = emailInput !== "" ? emailInput : null;
        var phone = phoneInput !== "" ? phoneInput : null;
        var address = addressInput !== "" ? addressInput : null;
        var gender = genderInput !== "" ? genderInput : null;
        var userName = userNameInput !== "" ? userNameInput : null;
        var identityCard = identityCardInput !== "" ? identityCardInput : null;

        var memberInfo = {
            "loginName": loginName, // "trong string la chuoi map voi field trong object class entity"
            "password": password,
            "email": email,
            "phone": phone,
            "address": address,
            "gender": gender,
            "userName": userName,
            "identityCard": identityCard
        };
        CommonAjax.post(memberInfo, "/user/register", addSuccess, addError);
    }

    function addSuccess(data) {
        var modal_header = $("#notifiModal .modal-header");
        var notify_modal = $("#notifiModal");

        modal_header.removeClass("btn-danger");
        modal_header.addClass("btn-success");
        notify_modal.addClass("fade");
        $('#massagemodal').html(data.message);//add message vao message của popup
        notify_modal.modal("toggle"); //hiển thị popup
        notify_modal.on('hidden.bs.modal', function () {
            location.reload();
        });
    }

    function addError(data) {
        var modal_header = $("#notifiModal .modal-header");
        var notify_modal = $("#notifiModal");

        modal_header.removeClass("btn-success");
        modal_header.addClass("btn-danger");
        notify_modal.addClass("fade");
        $('#massagemodal').html(data.responseJSON.message);//chuyển đổi message sang dạng Json của thư viện jquery
        notify_modal.modal("toggle");
        notify_modal.on('hidden.bs.modal', function () {
            $(".modal-backdrop").remove();
            $("#notifiModal").removeAttr("style");
        });
        $("div#notifiModal").modal('show');
    }
</script>


</body>
</html>    